<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include virtual="/Inc/AspBox/Cls_AB.asp" -->
<%
Response.Charset = "UTF-8"
Response.CodePage = 65001

AB.Use "Http"
Dim http, tmp, rule, arr, i
''=========================
''Demo 1 - 最简单的应用(Get)：
'直接获取页面源码
'tmp = AB.Http.Get("http://www.baidu.com/")
'AB.C.WR AB.C.HtmlEncode(tmp)
''=========================

''=========================
''Demo 2 - 最简单的Post：
'AB.Http.Data = Array("SearchClass:0","SearchKey:我吃西红柿")
'tmp = AB.Http.Post("http://www.paoshu8.com/Book/Search.aspx")
'AB.C.WR AB.C.HtmlEncode(tmp)
''=========================

''=========================
''Demo 3 - 通过属性配置：
'Set http = AB.Http.New()
''http.ResolveTimeout = 20000 '服务器解析超时时间，毫秒，默认20秒
''http.ConnectTimeout = 20000 '服务器连接超时时间，毫秒，默认20秒
''http.SendTimeout = 300000 '发送数据超时时间，毫秒，默认5分钟
''http.ReceiveTimeout = 60000 '接受数据超时时间，毫秒，默认1分钟
'http.Url = "http://www.paoshu8.com/Book/Search.aspx" '目标URL地址
'http.Method = "POST" 'GET 或者 POST, 默认GET
''目标文件编码，一般不用设置此属性，AB会自动判断目标地址的编码
''http.CharSet = "gb2312"
'http.Async = False '异步，默认False，建议不要修改
''数据提交方式一，如果是GET则会附在URL后以参数形式提交：
''http.Data = "SearchClass=0&SearchKey=" & Server.URLEncode("我吃西红柿")
''数据提交方式二，可以用Array参数的方式提交：
'http.Data = Array("SearchClass:0","SearchKey:我吃西红柿")
''http.User = "" '如果访问目标URL需要用户名
''http.Password = "" '如果访问目标URL需要密码
'http.Open
'AB.C.WE AB.C.HtmlEncode(http.Html)
'Set http = Nothing
''=========================

''=========================
''Demo 4 - 获取文件头：
'AB.Http.Get "http://www.baidu.com"
'tmp = AB.Http.Headers
'AB.C.WR AB.C.HtmlEncode(tmp)
''=========================

''=========================
''Demo 5 - 获取文件指定部分内容：
'Dim bookid,bookname,bookauthor,bookdesc,uptime,readlink
'bookid = 1639199
'AB.Http.Get("http://www.qidian.com/Book/"&bookid&".aspx")
''用SubStr按字符截取部分文本
'bookname = AB.Http.SubStr("</span>"&vbCrLf&" <h1>"&vbCrLf&" ","</h1>"&vbCrLf&" <b>小说作者：</b>",0)
'bookauthor = AB.Http.Select("<b>小说作者：<\/b>\s*<a [^>]+>\s*(.+)<\/a>","$1")
'bookdesc = AB.Http.SubStr("</div>"&vbCrLf&" <div class=""txt"">","</div>",0)
''用Find可按正则获取一段文本
'uptime = AB.Http.Find("更新时间：[\d- :]+")
''用Select可按正则编组选择匹配的部分文本,$0是获取正则匹配的字符串本身
'readlink = AB.Http.Select("(<a href="")(/BookReader/\d+\.aspx)(""[^>]*>\s*.+</a>)","$1http://www.qidian.com$2$3")
'AB.C.WR "<b>书名：</b>《" & bookname & "》 " & "<b>作者：</b>" & bookauthor & " " & uptime
'AB.C.WR "<b>阅读地址：</b>" & readlink
'AB.C.WR "<b>内容简介：</b>"
'AB.C.WR bookdesc
''=========================

''=========================
'Demo 6 - 获取文件循环部分：
AB.Http.Get "http://code.google.com/p/aspbox/downloads/list"
rule = "<tr onmouseover=""_rowRolloverOn\(this\)"" onmouseout=""_rowRolloverOff\(this\); cancelBubble=false"" class=""ifOpened"">\s*<td class=""vt"" nowrap=""nowrap"">\s+<a href=""([^""]+)""[^>]*?>[\s\S]+?<\/a>\s*<\/td>\s*<td class=""vt id col_0"">\s*<a href=""([^""]+)"">\s*(.+)\s*<\/a>\s*<\/td>\s*<td class=""vt col_1""[^>]*?><a [^>]*?>\s*(.+)\s*<\/a>[\s\S]*?<\/td>\s*<td class=""vt col_2""[^>]*?><a [^>]*?>\s*(.+)\s*<\/a><\/td>\s*<td class=""vt col_3""[^>]*?><a [^>]*?>\s*(.+)\s*<\/a><\/td>\s*<td class=""vt col_4""[^>]*?><a [^>]*?>\s*(.+)\s*<\/a><\/td>\s*<td class=""vt col_5""[^>]*?><a [^>]*?>\s*(.+)\s*<\/a><\/td>\s*<td>&nbsp;<\/td>\s*<\/tr>"
arr = AB.Http.Search(rule)
If UBound(arr)>=1 Then
AB.C.WR "====前"&UBound(arr)+1&"个匹配===="
For i = 0 To UBound(arr)
AB.C.WR "<b>第" & i + 1 & "个匹配项：</b>"
AB.C.WR AB.C.HtmlEncode(arr(i))
Next
AB.C.WR ""
End If
'还可以用正则来进行更复杂的应用
Dim Matches, Match
Set Matches = AB.C.RegMatch(AB.Http.Html,rule)
AB.C.WR "====最新文件列表===="
For Each Match In Matches
AB.C.WR AB.C.Format("<li><a href=""{1}"">▼</a> (<b>Filename</b>: <a href=""http://code.google.com/p/aspbox/downloads/{2}"">{3}</a>) (<b>Summary</b>: {4}) (<b>Uploaded</b>: {5}) (<b>ReleaseDate</b>: {6}) (<b>Size</b>: {7}) (<b>DownloadCount</b>: {8})</li>",Match)
Next
Set Matches = Nothing
''=========================

''=========================
''Demo 7 - 保存远程图片：
'AB.Http.Get "http://www.baidu.com"
'tmp = AB.Http.SaveImgTo_(AB.Http.Html, "imgatlocal/")
'AB.C.WR AB.C.HtmlEncode(tmp)
''=========================

''=========================
''Demo 8 - WebService(SOAP1.1)示例：
''获得腾讯QQ在线状态
'Dim QQ,xml : QQ = 517544292
'tmp = "<?xml version=""1.0"" encoding=""utf-8""?>"
'tmp = tmp & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">"
'tmp = tmp & " <soap:Body>"
'tmp = tmp & " <qqCheckOnline xmlns=""http://WebXml.com.cn/"">"
'tmp = tmp & " <qqCode>" & QQ & "</qqCode>"
'tmp = tmp & " </qqCheckOnline>"
'tmp = tmp & " </soap:Body>"
'tmp = tmp & "</soap:Envelope>"
'Set http = AB.Http.New
''设置请求头信息的三种方式，其一：
'http.RequestHeader("Host") = "www.webxml.com.cn"
''其二：
'http.SetHeader "Content-Type:text/xml; charset=utf-8"
''其三：
'http.SetHeader Array("Content-Length:" & Len(tmp), "SOAPAction:http://WebXml.com.cn/qqCheckOnline")
'http.Data = tmp
'tmp = http.Post("http://www.webxml.com.cn/webservices/qqOnlineWebService.asmx?WSDL")
'Set http = Nothing
''解析返回数据
'AB.Use "Xml"
'Set xml = AB.Xml.New
'xml.Load tmp
'tmp = xml("qqCheckOnlineResult").Value
'Set xml = Nothing
'Select Case tmp
'Case "Y" tmp = "在线"
'Case "N" tmp = "离线"
'Case "E" tmp = "号码错误"
'Case "A" tmp = "商业用户验证失败"
'CAse "V" tmp = "免费用户超过数量"
'End Select
'AB.C.WR "QQ:" & QQ & " (" & tmp & ")"

'=========================

AB.C.WR ""
AB.C.WR "------------------------------------"
AB.C.WR "页面执行时间： " & AB.ScriptTime & " 秒"
%>