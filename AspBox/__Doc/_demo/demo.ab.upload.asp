<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include virtual="/Inc/AspBox/Cls_AB.asp" -->
<%
AB.Debug = False
'AspBox 上传类 Demo，此例子为UTF-8编码，客户端进度条展示采用了jQuery，需要jQuery库支持
'友情提示：因为上传类的表单验证是要等文件上传完成之后才能做出判断，所以建议表单的验证请尽量同时做到客户端的验证，否则用户体验将打大折扣。
Response.Charset = "UTF-8"
'=====================================
Dim random, jsonFile, i, j, f, e
'载入核心类
AB.Use "Upload"
'使用无组件进度条（默认为不使用）
AB.Upload.UseProgress = True
'保存进度条数据临时文件的目录（默认为/__uptemp）
'AB.Upload.ProgressPath = "/__uptemp"
'仅允许上传文件类型(建议先在客户端判断)
AB.Upload.Allowed = "jpg|gif|png|rar|zip|txt"
'禁止上传的文件类型，如果设置了仅允许上传文件类型，则此设置不生效(建议先在客户端判断)
'AB.Upload.Denied = "exe|msi|bat|cmd|asp|asa"
'单个文件最大允许值，单位为KB（如果是图片建议在客户端判断）
AB.Upload.FileMaxSize = 1024*10
'全部文件最大允许值，单位为KB
AB.Upload.TotalMaxSize = 1024*30
'点击上传后执行
If AB.C.Get("act") = "upload" Then
'上传文件保存路径:
'比如：
'AB.Upload.SavePath = "/userFiles/"
'或者，可用<>带日期标志（参见AB.DateTime）按日期建立相应文件夹：
AB.Upload.SavePath = "uploadfiles/<yyyy>/<mm>/"
'保存时使用随机文件名，默认为False，即不使用
AB.Upload.Random = True
'是否自动建立不存在的文件夹，默认为True，即会自动建立
'AB.Upload.AutoMD = False
'获取上传的唯一KEY用于生成进度条数据Json文件给js调用
AB.Upload.Key = AB.C.Get("json")
'重要方法：开始上传
AB.Upload.StartUpload()
'保存全部上传文件
AB.Upload.SaveAll
'或者保存单个文件：
'AB.Upload.File("file1").Save
'或者保存单个文件到具体路径(另存为)：
'AB.Upload.File("file1").SaveAs AB.Upload.File("file1").NewPath & "这是新文件名." & AB.Upload.File("file1").Ext
AB.C.WR "<ul>"
'取表单项的值，仍然用AB.Post^_^，还有一种用法是AB.Upload.Form("nick")，但是就没有Post的多参数啦：
AB.C.WR "<li>用户名 : " & AB.C.Post("nick:s") & "</li>"
'表单项是集合，可以遍历：
For Each i In AB.Upload.Form
AB.C.WR "<li>表单项 '" & i & "' 的值 : " & AB.Upload.Form(i) & "</li>"
Next
AB.C.WR "</ul>"
AB.C.WR "共 " & AB.Upload.File.Count & " 个文件上传框，本次上传成功了 " & AB.Upload.Count & " 个文件。成功上传的文件信息如下："
AB.C.WR "================="
'所有的上传文件也是一个集合，同样可以遍历：
For Each j In AB.Upload.File
Set f = AB.Upload.File(j)
'只列出上传的文件
If f.Size>0 Then
AB.C.WR "文件原位置：" & f.Client
AB.C.WR "文件原目录：" & f.OldPath
AB.C.WR "文件大小：" & f.Size
AB.C.WR "文件名称：" & f.Name
AB.C.WR "文件扩展名：" & f.Ext
AB.C.WR "文件MIME类型：" & f.MIME
AB.C.WR "新路径："& f.NewPath
AB.C.WR "新名称："& f.NewName
AB.C.WR "Web路径：" & f.WebPath & Server.URLEncode(f.NewName)
AB.C.WR "================="
End If
Next
'显式的释放，才能删除进度条数据文件
Set AB.Upload = Nothing
AB.C.WE "<a href=""./demo.asp"">继续上传</a>"
End If
'生成本次上传的唯一KEY
random = AB.Upload.GenKey
'获取给js使用的Json文件的地址
jsonFile = AB.Upload.ProgressFile(random)
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>AspBox Upload Demo</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<style type="text/css">
<!--
/*表单样式*/
.upload {font-size:14px; font-family:Tahoma;width:500px; padding:20px 20px;}
.upload p{ padding:0; margin:0 0 10px 0;}
.upload input{ font-size:12px;font-family:Tahoma; padding:4px;}
.upload input.ipt{ width:436px;}
.upload input.ipts{ width:180px;}
.upload .btns{ padding:10px 0;}
/*进度条样式*/
.upload #formUpload{position:relative;}
.upload .progress, .upload #progress { font-size:12px;position:absolute;top:80px;left:256px;height:130px;width:240px; background-color:#E4E4E4;}
.upload .progress { line-height:130px; text-align:center;}
.upload #progress {display:none; color:#036;}
.upload #progress .info{padding:10px 0;text-align:center; line-height:1.5em;}
.upload .progress-bar{border:1px solid #069; position:absolute; height:14px; width:200px;left:20px; bottom:10px;}
.upload #uploadPercent{position:absolute;top:0;left:0;width:100%;font-size:11px;text-align:center;}
.upload #progressBar{position:absolute;top:0;left:0;width:0; height:14px;background-color:#0099E3;}
-->
</style>
</head>
<body>
<fieldset class="upload"><legend>AB.Upload上传文件示例 （多文件带进度条）</legend>
<!-- form的method是post，enctype是multipart/form-data，这在上传表单里是必需的 -->
<form id="formUpload" method="post" enctype="multipart/form-data">
<p>昵 称：<input type="text" name="nick" class="ipt" /></p>
<p>密 码：<input type="password" name="pwd" class="ipt" /></p>
<p>附件1：<input name="file1" type="file" /></p>
<p>附件2：<input name="file2" type="file" /></p>
<p>附件3：<input name="file3" type="file" /></p>
<p>附件3：<input name="file4" type="file" /></p>
<p>允许上传类型：<%= Replace(AB.Upload.Allowed,"|", ", ") %></p>
<p>单个文件最大允许值：<%= AB.Upload.FileMaxSize %> KB</p>
<p>全部文件最大允许值：<%= AB.Upload.TotalMaxSize %> KB</p>
<div class="btns"><input type="submit" id="btnSubmit" value="确认提交"/></div>
<div class="progress">请选择一个或多个要上传的文件！</div>
<div id="progress">
<div class="info">
<strong>正在上传，请稍候…</strong><br />
总大小：<span id="uploadTotalSize">0 KB</span> / 已上传：<span id="uploadSize">0 KB</span><br />
上传速度：<span id="uploadSpeed">0 KB/S</span><br />
总共时间：<span id="uploadTotalTime">00:00:00</span><br />
剩余时间：<span id="uploadRemainTime">00:00:00</span>
</div>
<div class="progress-bar">
<div id="progressBar"></div>
<div id="uploadPercent">0%</div>
</div>
</div>
</form>
</fieldset>
</body>
<script language="javascript">
$('#formUpload').submit(function(){
//$('#progress').show();
//return false;
var flag = false;
$(this).find(':file').each(function(){
if ($(this).val()!=''){
flag = true;
return false;
}
});
if (!flag) {
alert('请至少上传一个文件！');
return false;
} else {
//在Form的action中加入上传的唯一KEY
this.action = 'demo.asp?act=upload&json=<%=random%>';
//显示进度条
startProgress('<%=jsonFile%>');
return true;
}
});

//显示进度条
function startProgress(path){
//0.5秒后启动进度条
setTimeout('readProgress("' + path + '")',500);
}
//读取进度条
function readProgress(path){
var percent = 0;
try{
//Ajax读取
$.get(path,{rnd:Math.floor(Math.random()*10000)},function(d){
//解析Json
var progress = eval('('+d+')');
//已上传大小 和 总大小
$('#uploadTotalSize').text(progress.total);
$('#uploadSize').text(progress.uploaded);
//上传速度
$('#uploadSpeed').text(progress.speed);
//上传总共需要时间（估计值）
$('#uploadTotalTime').text(progress.totaltime);
//上传剩余时间
$('#uploadRemainTime').text(progress.remaintime);
//上传百分比，更新显示状态
percent = progress.percent;
$('#uploadPercent').text(percent+'%');
$('#progressBar').css({'width':percent*2+'px'});
});
} catch(e){ }
//上传如未完成继续刷新(时间为0.5秒刷新一次)
if (percent<100){
setTimeout('readProgress("'+path+'")',500);
//显示进度条
$('#progress').show();
} else {
$('.progress').text('上传成功！正在保存文件…');
$('#progress').hide();
}
}
</script>
</html>
<%
Set AB.Upload = Nothing
%>