<%
'######################################################################
'## ab.up.u5x.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Up-u5x Class (化境HTTP上传类 upload_5xsoft.inc)
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/05/04 0:00
'## Description :   化境HTTP上传程序2.0 For AspBox
'## -------------------------------------------------------------------
'## 调用方法：				
'##   ab.use "up" : ab.up.use "u5x"
'##   'set upload = new cls_ab_up_u5x
'##   set upload = ab.up.u5x
'##   upload.init() '起始执行函数(必须提前调用!!!)
'######################################################################

Dim Abx_Data_5xsoft
Class Cls_AB_Up_u5x
	public objForm,objFile,Version
	private tStream,RequestData,sStart,vbCrlf,sInfo,iInfoStart,iInfoEnd,iStart
	private TotalBytes,sFormValue,sFileName,theFile
	private iFindStart,iFindEnd,iFormStart,iFormEnd,sFormName
	private iFileSize,sFilePath,sFileType
	Private s_charset

	Private Sub Class_Initialize
		On Error Goto 0
		Version="化境HTTP上传程序 Version 2.0"
		s_charset = AB.CharSet
		Set objForm=Server.CreateObject(AB.dictName)
		Set objFile=Server.CreateObject(AB.dictName)
		Set Abx_Data_5xsoft = Server.CreateObject(AB.steamName)
		Set tStream = Server.CreateObject(AB.steamName)
		AB.Use "Form"
	End Sub

	Public Sub Init() '起始执行函数(必须提前调用!!!)
		Call DataInit()
	End Sub

	Private Sub Class_Terminate
		'IF Request.TotalBytes>0 then
			objForm.RemoveAll
			objFile.RemoveAll
			Set objForm=Nothing
			Set objFile=Nothing
			Abx_Data_5xsoft.Close
			Set Abx_Data_5xsoft =Nothing
			Err.Clear
		'End IF
	End Sub

	Public Function [New]()
		Set [New] = New Cls_AB_Up_u5x
	End Function

	Public Sub DataInit()
		AB.Form.Up = "up.u5x"
		If Request.TotalBytes<1 Then Exit Sub
		TotalBytes = Request.TotalBytes
		Dim BytesRead,ChunkReadSize,PartSize,DataPart,tempdata
		Dim vMode : vMode = 1
		If vMode = 0 Then
			Abx_Data_5xsoft.Type = 1
			Abx_Data_5xsoft.Mode = 3
			Abx_Data_5xsoft.Open
			Abx_Data_5xsoft.Write Request.BinaryRead(TotalBytes)
			Abx_Data_5xsoft.Position=0
			tempdata = Abx_Data_5xsoft.Read
		Else
			Abx_Data_5xsoft.Type = 1
			Abx_Data_5xsoft.Mode = 3
			Abx_Data_5xsoft.Open
			BytesRead = 0
			ChunkReadSize = 1024 * 16
			Do While BytesRead < TotalBytes
				PartSize = ChunkReadSize
				If PartSize + BytesRead > TotalBytes Then PartSize = TotalBytes - BytesRead
				DataPart = Request.BinaryRead(PartSize)
				Abx_Data_5xsoft.Write DataPart
				BytesRead = BytesRead + PartSize
			Loop
			Abx_Data_5xsoft.Position = 0
			tempdata = Abx_Data_5xsoft.Read
		End If
		RequestData = tempdata
		'If IsNull(RequestData) Then RequestData=""
		iFormStart = 1
		iFormEnd = LenB(RequestData)
		vbCrlf = ChrB(13) & ChrB(10)
		sStart = MidB(RequestData,1,InStrB(iFormStart,RequestData,vbCrlf)-1)
		iStart = LenB(sStart)
		iFormStart = iFormStart+iStart+1
		while (iFormStart + 10) < iFormEnd
			iInfoEnd = InStrB(iFormStart,RequestData,vbCrlf & vbCrlf)+3
			tStream.Type = 1
			tStream.Mode = 3
			tStream.Open
			Abx_Data_5xsoft.Position = iFormStart
			Abx_Data_5xsoft.CopyTo tStream,iInfoEnd-iFormStart
			tStream.Position = 0
			tStream.Type = 2
			tStream.Charset = s_charset
			sInfo = tStream.ReadText
			tStream.Close
			'取得表单项目名称
			iFormStart = InStrB(iInfoEnd,RequestData,sStart)
			iFindStart = InStr(22,sInfo,"name=""",1)+6
			iFindEnd = InStr(iFindStart,sInfo,"""",1)
			sFormName = Lcase(Mid(sinfo,iFindStart,iFindEnd-iFindStart))
			'如果是文件
			if InStr(45,sInfo,"filename=""",1) > 0 then
				Set theFile = New AB_u5x_FileInfo
				'取得文件名
				iFindStart = InStr(iFindEnd,sInfo,"filename=""",1)+10
				iFindEnd = InStr(iFindStart,sInfo,"""",1)
				sFileName = Mid(sinfo,iFindStart,iFindEnd-iFindStart)
				theFile.FileName=u5x_getFileName(sFileName)
				theFile.FilePath=u5x_getFilePath(sFileName)
				'取得文件类型
				iFindStart = InStr(iFindEnd,sInfo,"Content-Type: ",1)+14
				iFindEnd = InStr(iFindStart,sInfo,vbCr)
				theFile.FileType = Mid(sinfo,iFindStart,iFindEnd-iFindStart)
				theFile.FileStart =iInfoEnd
				theFile.FileSize = iFormStart-iInfoEnd-3
				theFile.FormName = sFormName
				if not objFile.Exists(sFormName) then
					objFile.add sFormName,theFile
				End if
				Set theFile = Nothing
			else
			'如果是表单项目
				tStream.Type =1
				tStream.Mode =3
				tStream.Open
				Abx_Data_5xsoft.Position = iInfoEnd
				Abx_Data_5xsoft.CopyTo tStream,iFormStart-iInfoEnd-3
				tStream.Position = 0
				tStream.Type = 2
				tStream.Charset = s_charset
				sFormValue = tStream.ReadText
				tStream.Close
				if objForm.Exists(sFormName) and Trim(sFormName)<>"" then
					objForm(sFormName)=objForm(sFormName)&", "&sFormValue
				else
					objForm.Add sFormName,sFormValue
				end if
			end if
			iFormStart=iFormStart+iStart+1
		wend
		RequestData=""
		Set tStream = Nothing
	End Sub

	Public Function Form(strForm)
		strForm=lcase(strForm)
		if not objForm.exists(strForm) then
		  Form=""
		else
		  Form=objForm(strForm)
		end if
	End Function

	Public Function File(strFile)
		strFile=lcase(strFile)
		if not objFile.exists(strFile) then
		  Set File = New AB_u5x_FileInfo
		else
		  Set File=objFile(strFile)
		end if
	End Function

	Private Function u5x_getFilePath(FullPath)
		If FullPath <> "" Then
			u5x_getFilePath = Left(FullPath,InStrRev(FullPath, "\"))
		Else
			u5x_getFilePath = ""
		End If
	End Function

	Private Function u5x_getFileName(FullPath)
		If FullPath <> "" Then
			u5x_getFileName = mid(FullPath,InStrRev(FullPath, "\")+1)
		Else
			u5x_getFileName = ""
		End If
	End Function

	'以下补充拓展 (2011-07-19)

	Function CheckFileExt(FileName,ExtName) '判断文件类型是否合格
		FileType = ExtName
		FileType = Split(FileType,",")
		For i = 0 To Ubound(FileType)
			If LCase(Right(FileName,3)) = LCase(FileType(i)) then
				CheckFileExt = True
				Exit Function
			Else
				CheckFileExt = False
			End if
		Next
	End Function

	Function CheckAndCreateFolder(FolderName) '检查文件夹是否存在，不存在则创建文件夹
		Dim FolderPath
		FolderPath = FolderName
		'FolderPath = Server.Mappath(FolderName)
		Dim fso
		Set fso = CreateObject(AB.fsoName)
		  If Not fso.FolderExists(fldr) Then
			fso.CreateFolder(fldr)
		  End If
		Set fso = Nothing
	End Function

	Function CheckFileExists(FileName) '检查文件是否存在，重命名存在文件
		Dim fso
		Set fso=Server.CreateObject(AB.fsoName)
		If fso.FileExists(SaveFile) Then
		Dim i,msg:i=1:msg=True
		Do While msg
			CheckFileExists = Replace(SaveFile, Right(SaveFile,4), "_"&i&Right(SaveFile,4))
			If not fso.FileExists(CheckFileExists) Then
				msg=False
			End If
			i=i+1
		Loop
		Else
			CheckFileExists = FileName
		End If
		Set fso=Nothing
	End Function

End Class

'Modify By Lajox (2011-07-19)
Class AB_u5x_FileInfo
	Public FormName,FileName,FilePath,FileSize,FileType,FileStart
	Private isUploaded

	Private Sub Class_Initialize
		FileName = ""
		FilePath = ""
		FileSize = 0
		FileStart= 0
		FormName = ""
		FileType = ""
		isUploaded = False
	End Sub

	Private Sub Class_Terminate

	End Sub

	Public Function SaveAs(fullPath)
		On Error Resume Next
		dim dr,ErrorChar,i
		isUploaded=False
		if trim(fullpath)="" or FileStart=0 or FileName="" or right(fullpath,1)="/" then:SaveAs=True:Exit Function:end if
		Set dr=CreateObject(AB.SteamName)
		dr.Mode=3
		dr.Type=1
		dr.Open
		Abx_Data_5xsoft.position=FileStart
		Abx_Data_5xsoft.copyto dr,FileSize
		dr.SaveToFile fullPath,2
		dr.Close
		Set dr=nothing
		isUploaded=True
		SaveAs=isUploaded
		On Error Goto 0
   End Function
End Class
%>