<%
'###########################################################################################
'## ab.mail.asp
'## ----------------------------------------------------------------------------------------
'## Feature     :   AspBox Mail Class
'## Version     :   v1.0.1
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2011/11/02 14:48
'## Description :   AspBox Jmail邮件发送（插件）
'##     建议Body 和HTMLBody只设置其中一个，否则邮件将是多部分MIME格式的消息
'##     本类是基于Jmail组件的类，在Jmail 4.5 版本下测试通过，有兴趣的可以测试下其他版本。
'##     如果您的服务器环境不支持Jmail，将无法使用此程序。
'## ========================================================================================
'## Examples :
'##     '1.快速发送邮件方式，前面需要配置了各项基本参数，适用于邮件参数基本固定，只需要添加收件人和附件的情况
'##     Dim oMail : Set oMail = AB.Lib("mail")
'##     oMail.Init()  '初始化jmail对象，因为下面的设置都需要涉及到jmail对象，为了让下面的参数设置顺序可以随意排列必须先初始化
'##     oMail.AddRecipient "********@126.com"  '增加联系人
'##     oMail.AddRecipient "********@126.com;********@126.com"  '增加多个联系人
'##     oMail.AddRecipient Array("********@126.com")  '数组方式增加联系人，数组也可以不这样设置，只要是数组就可以
'##     oMail.AddRecipientBCC "********@126.com"  '增加密件收件人,非必须
'##     oMail.AddRecipientCC "********@126.com"  '增加邮件抄送者,非必须
'##     oMail.Smtp="smtp.126.com" '设置SMTP
'##     oMail.From="********@126.com" '发送人的邮箱
'##     oMail.FromName="虚幻" '发送人姓名
'##     oMail.MailServerUserName="********@126.com" '发送人邮箱用户名
'##     oMail.MailServerPassword="********" '发送人邮箱密码
'##     oMail.Subject="Subjectsmtp.126.com" '邮件主题
'##     oMail.Body="<font color='red'>Bodysmtp.126.com</font>" '邮件body内容
'##     oMail.AppendText "<font color='red'>AppendText.126.com</font>" '增加文本内容
'##     oMail.AddAttachmentIn("t.rar;t.asp") '增加多个嵌入式附件
'##     Dim t_file : t_file=oMail.File("") '返回附件数组，非必须
'##     AB.C.PrintCn t_file(0) & "<br>" '输出第一个附件ID
'##     AB.C.PrintCn oMail.File(1) & "<br>" '输出第二个附件ID，如果
'##     oMail.HTMLBody="<font color='red'>HTMLBodysmtp.126.com</font>" '邮件HTMLBODY内容
'##     oMail.AppendHTML oMail.File(0) '往邮件内容中添加附件，有附件的邮件将以html格式发送
'##     oMail.AddAttachment("t.rar") '增加一个普通附件
'##     AB.C.PrintCn oMail.QuickSend() & "<br>" '快速发送，输出发送邮件数
'##     AB.C.PrintCn oMail.RecipientsCount() & "<br>" '输出收件人数量
'##     '2.一般发送邮件方式，不需要上面的设置，只需要一个函数就可以了，邮件和附件参数不符都可以采用字符串或数组形式，字符串形式的话多个数据之间用 ; 进行分隔。
'##     AB.C.PrintCn oMail.Send("smtp.126.com","********@126.com","虚幻","********@126.com","********@126.com","********","Subject","","<font color='red'>HTMLBody</font>",1,"","","") '发送邮件，输出发送邮件数
'## 该函数的参数如下，每个参数的具体含义可以详见该函数头部的注释AB.Lib("mail").Send(Smtp,From,FromName,Email,MailServerUserName,MailServerPassword,Subject,Body,HTMLBody,Priority,Silent,tCharset,tContentType)
'##     基本的使用主要就是上面的了，其他一些应用可以详见代码，每个函数和方法都有注释了。
'######################################################################

Class Cls_AB_Mail

	' ============================================
	' 变量声明
	' ============================================
	Private s_jmail,s_ISOEncodeHeaders,s_Silent,s_Charset,s_ContentType,s_From,s_FromName,s_MailServerUserName,s_MailServerPassword,s_Priority,s_Logging,s_Smtp
	Private t_Subject,t_Body,t_HTMLBody,t_File
	Private o_jmail

	' ============================================
	' 类模块初始化
	' ============================================
	Private Sub Class_Initialize
		s_jmail					= "JMail.Message"  'JMail组件名称
		s_ISOEncodeHeaders		= True   '是否将信头编码成iso-8859-1字符集. 缺省是True
		s_Silent				= True   '设置为true,ErrorCode包含的是错误代码
		s_Charset				= AB.CharSet  '设置标题和内容编码，如果标题有中文，必须设定编码为utf-8
		s_ContentType			= "text/html"  '如果发内嵌附件设置为空值
		s_From					= ""  ' 发送者地址
		s_FromName				= ""  ' 发送者姓名
		s_MailServerUserName	= ""  ' 身份验证的用户名
		s_MailServerPassword	= ""  ' 身份验证的密码
		s_Priority				= 1   '设置优先级，范围从1到5，越大的优先级越高，3为普通
		s_Logging				= True   '是否使用日志
		s_Smtp					= ""
		t_Subject				= ""
		t_Body					= ""
		t_HTMLBody				= ""
		t_File					= ""
		AB.Error(10000)			= "邮件发送成功."
		AB.Error(10001)			= "您的服务器不支持该组件."
		AB.Error(10002)			= "发送者地址不能为空."
		AB.Error(10003)			= "发送者姓名不能为空."
		AB.Error(10004)			= "优先级必须为1-5之间的数字."
		AB.Error(10005)			= "发送邮件对象未创建."
		AB.Error(10006)			= "邮件地址不正确."
		AB.Error(10007)			= "SMTP地址为空或不正确."
		AB.Error(10008)			= "没有任何收件人."
		AB.Error(20001)			= "身份验证的用户名不能为空."
		AB.Error(20002)			= "身份验证的密码不能为空."
		AB.Error(30001)			= "邮件发送失败."
		AB.Error(30002)			= "邮件设置参数出错."
	End Sub

	' ============================================
	' 类终结
	' ============================================
	Private Sub Class_Terminate
		On ErrOr Resume Next
		If IsObject(o_jmail) Then
			o_jmail.Close()
			Set o_jmail = Nothing
		End If
		On Error Goto 0
	End Sub

	' ============================================
	' 初始化Jmail对象
	' ============================================
	Public Sub Init()
		If Not AB.C.IsInstall(s_jmail) then AB.Error.Raise 10001
		If Not IsObject(o_jmail) Then Set o_jmail = Server.CreateObject(s_jmail)
	End Sub

	' ============================================
	' 关闭释放Jmail对象
	' ============================================
	Public Sub FreeJmail()
		If IsObject(o_jmail) then
			o_jmail.close()
			Set o_jmail   =  Nothing
		End If
	End Sub

	' ============================================
	' 设置ISOEncodeHeaders
	' ============================================
	Public Property Let ISOEncodeHeaders(ByVal p)
		s_ISOEncodeHeaders = AB.C.IIF(p,p,False)
	End Property

	' ============================================
	' 返回ISOEncodeHeaders
	' ============================================
	Public Property Get ISOEncodeHeaders()
		ISOEncodeHeaders = s_ISOEncodeHeaders
	End Property

	' ============================================
	' 设置Silent
	' ============================================
	Public Property Let Silent(ByVal p)
		s_Silent = AB.C.IIF(p,p,False)
	End Property

	' ============================================
	' 返回Silent
	' ============================================
	Public Property Get Silent()
		Silent = s_Silent
	End Property

	' ============================================
	' 设置Logging
	' ============================================
	Public Property Let Logging(ByVal p)
		s_Logging = AB.C.IIF(p,p,False)
	End Property

	' ============================================
	' 返回Logging
	' ============================================
	Public Property Get Logging()
		Logging = s_Logging
	End Property

	' ============================================
	' 设置Charset
	' ============================================
	Public Property Let [Charset](ByVal p)
		s_Charset = AB.C.Ifhas(p,AB.CharSet)
	End Property

	' ============================================
	' 设置ContentType
	' ============================================
	Public Property Let [ContentType](ByVal p)
		s_ContentType = AB.C.Ifhas(p,"text/html")
	End Property

	' ============================================
	' 设置From
	' ============================================
	Public Property Let From(ByVal p)
	s_From = p
	End Property

	' ============================================
	' 设置FromName
	' ============================================
	Public Property Let FromName(ByVal p)
		s_FromName = p
	End Property

	' ============================================
	' 设置MailServerUserName
	' ============================================
	Public Property Let MailServerUserName(ByVal p)
	s_MailServerUserName = p
	End Property

	' ============================================
	' 设置MailServerPassword
	' ============================================
	Public Property Let MailServerPassword(ByVal p)
		s_MailServerPassword = p
	End Property

	' ============================================
	' 设置Priority
	' ============================================
	Public Property Let Priority(ByVal p)
		If Not AB.C.Test(p,"int") or int(p)<1 or int(p)>5 Then AB.Error.Raise 10004
		s_Priority = p
	End Property

	' ============================================
	' 设置Smtp
	' ============================================
	Public Property Let Smtp(ByVal p)
		s_Smtp=p
	End Property

	' ============================================
	' 设置Subject
	' ============================================
	Public Property Let Subject(ByVal p)
		t_Subject = p
	End Property

	' ============================================
	' 设置Body
	' ============================================
	Public Property Let Body(ByVal p)
		t_Body = p
	End Property

	' ============================================
	' 设置HTMLBody
	' ============================================
	Public Property Let HTMLBody(ByVal p)
		t_HTMLBody = p
	End Property

	' ============================================
	' 返回日志
	' ============================================
	Public Property Get [Log]()
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		[Log] = o_jmail.Log
	End Property

	' ============================================
	' 返回嵌入式附件列表,如果参数为空或者小于0则返回附件列表数组，否则则返回指定索引值的附件值，如果指定索引值大于最大数组下标则返回最大下标的附件值
	' ============================================
	Public Property Get [File](ByVal p)
		If Not AB.C.Has(p) Then P=-1
		If Not IsNumeric(p) Then p=Int(p)
		If p < 0 Then
			[File] = t_File
		Else
			[File] = t_File(AB.C.IIF(p > Ubound(t_File),Ubound(t_File),p))
		End If
	End Property

	' ============================================
	' 返回所有收件人数量
	' ============================================
	Public Property Get RecipientsCount()
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		RecipientsCount=o_jmail.Recipients.count
	End Property

	' ============================================
	' 清除所有收件人
	' ============================================
	Public Sub RecipientsClear()
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		o_jmail.Recipients.clear()
	End Sub

	' ============================================
	' 创建Jmail对象,返回一个新的jmail对象,提供需要直接使用jmail对象的情况下使用
	' ============================================
	Public Function Jmail()
		If Not AB.C.IsInstall(s_jmail) then AB.Error.Raise 10001
		Set Jmail = Server.CreateObject(s_jmail)
	End Function

	' ============================================
	' 添加收件人,参数可以为字符串，多个地址之间用;分隔，也可以为数组
	' ============================================
	Public Sub AddRecipient(ByVal p)
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		t_p=ToArray(p)
		Dim i
		For i=0 to UBound(t_p)
			If Not AB.C.Test(t_p(i),"email") Then AB.Error.Raise 10006
			o_jmail.AddRecipient(t_p(i))
		Next
	End Sub

	' ============================================
	' 添加密件收件人的地址
	' ============================================
	Public Function AddRecipientBCC(ByVal p)
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		t_p = ToArray(p)
		Dim i
		For i=0 to UBound(t_p)
			If Not AB.C.Test(t_p(i),"email") Then
				AB.Error.Raise 10006
			End If
			o_jmail.AddRecipientBCC(t_p(i))
		Next
	End Function

	' ============================================
	' 添加邮件抄送者的地址
	' ============================================
	Public Function AddRecipientCC(ByVal p)
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		t_p = ToArray(p)
		Dim i
		For i=0 to UBound(t_p)
			If Not AB.C.Test(t_p(i),"email") Then AB.Error.Raise 10006
			o_jmail.AddRecipientCC(t_p(i))
		Next
	End Function

	' ============================================
	' 增加普通附件,参数可以为相对和绝对地址，可以为字符串，也可以为数组
	' ============================================
	Public Sub AddAttachment(ByVal p)
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		t_p=ToArray(p)
		Dim i
		For i=0 to UBound(t_p)
			o_jmail.AddAttachment(Server.MapPath(t_p(i)))
		Next
	End Sub

	' ============================================
	' 返回附件数量
	' ============================================
	Public Function AttachmentsCount()
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		AttachmentsCount=o_jmail.Attachments.Count
	End Function

	' ============================================
	' 清除所有附件
	' ============================================
	Public Sub AttachmentsClear()
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		o_jmail.Attachments.Clear
	End Sub

	' ============================================
	' 增加嵌入式附件,参数可以为相对和绝对地址，可以为字符串，也可以为数组，返回文件列表数组
	' ============================================
	Public Function AddAttachmentIn(ByVal p)
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		t_p = ToArray(p)
		Redim t_File(Ubound(t_p))
		Dim i
		For i=0 to UBound(t_p)
			t_File(i) = o_jmail.AddAttachment(Server.MapPath(t_p(i)))
		Next
		AddAttachmentIn = t_File
	End Function

	' ============================================
	' 追加HTML
	' ============================================
	Public Sub AppendHTML(ByVal p)
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		o_jmail.appendHTML p
	End Sub

	' ============================================
	' 追加文本
	' ============================================
	Public Sub AppendText(ByVal p)
		If Not isobject(o_jmail) Then AB.Error.Raise 10005
		o_jmail.appendText p
	End Sub

	' ============================================
	' 快速发送邮件，返回发送邮件数量，配置了各项参数的情况下才能使用，适用于邮件参数基本固定，只需要添加收件人和附件
	' ============================================
	Public Function QuickSend()
		If Not AB.C.IsInstall(s_jmail) Then AB.Error.Raise 10001
		If Not AB.C.Has(s_Smtp) Then AB.Error.Raise 10007
		If Not AB.C.Has(s_From) Then AB.Error.Raise 10002
		If Not AB.C.Has(s_FromName) Then AB.Error.Raise 10003
		If Not AB.C.Has(s_MailServerUserName) Then AB.Error.Raise 20001
		If Not AB.C.Has(s_MailServerPassword) Then AB.Error.Raise 20002
		If Not AB.C.Has(t_Subject) Then t_Subject = "无主题."
		If RecipientsCount()=0 Then AB.Error.Raise 10008
		If Not isobject(o_jmail) Then Set o_jmail = Server.CreateObject(s_jmail)
		o_jmail.silent = s_Silent
		o_jmail.Charset = s_Charset
		If AttachmentsCount()=0 Then o_jmail.ContentType = s_ContentType
		o_jmail.From = s_From
		o_jmail.FromName = s_FromName
		o_jmail.MailServerUserName = s_MailServerUserName
		o_jmail.MailServerPassword = s_MailServerPassword
		o_jmail.Subject = t_Subject
		o_jmail.Body = t_Body
		o_jmail.HTMLBody = t_HTMLBody
		o_jmail.Priority = s_Priority
		o_jmail.Send s_Smtp
		If Err.Number<>0 then AB.Error.Raise 30001
		QuickSend = RecipientsCount()
		FreeJmail()
	End Function

	' ============================================
	' 根据参数发送邮件，成功返回发送数量,收件人参数可以为字符串，数组
	' Smtp 'smtp地址
	' From '发件人邮箱
	' FromName '发件人姓名
	' Email '收件人邮箱
	' MailServerUserName '身份验证的用户名
	' MailServerPassword '身份验证的密码
	' Subject '主题
	' Body '内容
	' HTMLBody 'HTML格式内容
	' Priority '优先级
	' Silent '设置为true,ErrorCode包含的是错误代码
	' tCharset '设置标题和内容编码
	' tContentType '如果发内嵌附件设置为空值
	' ============================================
	Public Function Send(ByVal Smtp,ByVal From,ByVal FromName,ByVal Email,ByVal MailServerUserName,ByVal MailServerPassword,ByVal Subject,ByVal Body,ByVal HTMLBody,ByVal Priority,ByVal Silent,ByVal tCharset,ByVal tContentType)
		If Not AB.C.IsInstall(s_jmail) Then AB.Error.Raise 10001
		Set t_jmail = Server.CreateObject(s_jmail)
		If Not AB.C.Has(Smtp) Then AB.Error.Raise 10007
		If Not AB.C.Has(From) Then AB.Error.Raise 10002
		If Not AB.C.Has(FromName) Then AB.Error.Raise 10003
		If Not AB.C.Has(Email) Then AB.Error.Raise 10008
		Dim t_Email:t_Email=ToArray(Email)
		Dim i
		For i=0 to UBound(t_Email)
			If Not AB.C.Test(t_Email(i),"email") Then AB.Error.Raise 10006
			t_jmail.AddRecipient(t_Email(i))
		Next
		If t_jmail.Recipients.count=0 Then AB.Error.Raise 10008
		If Not AB.C.Has(MailServerUserName) Then AB.Error.Raise 20001
		If Not AB.C.Has(MailServerPassword) Then AB.Error.Raise 20002
		If Not AB.C.Has(Subject) Then Subject="无主题."
		If Not AB.C.Has(Priority) Then Priority = 1
		If Not AB.C.Has(Silent) Then Silent = True
		If Not AB.C.Has(tCharset) Then tCharset = AB.CharSet
		If Not AB.C.Has(tContentType) Then tContentType = "text/html"
		t_jmail.silent = Silent
		t_jmail.Charset = tCharset
		't_jmail.Logging = s_Logging
		t_jmail.ContentType = tContentType
		t_jmail.From = From
		t_jmail.FromName = FromName
		t_jmail.MailServerUserName = MailServerUserName
		t_jmail.MailServerPassword = MailServerPassword
		t_jmail.Subject = Subject
		t_jmail.Body = Body
		t_jmail.HTMLBody = HTMLBody
		t_jmail.Priority = Priority
		t_jmail.Send Smtp
		If Err.Number<>0 Then AB.Error.Raise 30001
		Send = t_jmail.Recipients.count
		t_jmail.close()
		Set t_jmail = Nothing
	End Function

	' ============================================
	' 字符串转数组，字符串各值以;分隔，如果参数为数组则直接返回数组
	' ============================================
	Public Function ToArray(ByVal p)
		If Not IsArray(p) then
			If InStr(p,";")>0 Then
				If InStrRev(p,";")=Len(p) Then p = Left(p,len(p)-1)
				ToArray = Split(p,";")
			Else
				ToArray = Array(p)
			End If
		Else
			ToArray = p
		End If
	End Function

End Class
%>