<%
'######################################################################
'## ctrl.cache.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc Cache-Ctrl Block
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/06/10 23:10
'## Description :   AspBox Mvc Cache-Ctrl Block(MVC框架之Cache缓存控制模块)
'######################################################################

Class Cls_Ctrl_Cache

	Private	s_mark
	Private i_expires

	Private Sub Class_Initialize()
		s_mark = ""
		i_expires = 3600
	End Sub

	Private Sub Class_Terminate()

	End Sub

	'@ *****************************************************************************************
	'@ 过程名:  Ctrl.Cache.Mark [= s]
	'@ 返  回:  --
	'@ 作  用:  设置/获取 Cache缓存名称前缀
	'==DESC=====================================================================================
	'@ 参数 s(可选): String (字符串)
	'==DEMO=====================================================================================
	'@ Ctrl.Cache.Mark = "abxss_"
	'@ AB.C.PrintCn "Cache名称前缀:" & Ctrl.Cache.Mark
	'@ *****************************************************************************************

	Public Property Let Mark(Byval s)
		s_mark = s
	End Property

	Public Property Get Mark()
		Mark = s_mark
	End Property

	'@ *****************************************************************************************
	'@ 过程名:  Ctrl.Cache.Expires [= n]
	'@ 返  回:  --
	'@ 作  用:  设置/获取 Cache缓存过期时间(秒)
	'==DESC=====================================================================================
	'@ 参数 n(可选): Integer (整数) 即几秒后过期
	'==DEMO=====================================================================================
	'@ Ctrl.Cache.Expires = 3600 '设置缓存3600秒后过期
	'@ Ctrl.Cache.Set "ca_admin", "admin", ""
	'@ *****************************************************************************************

    Public Property Let [Expires](Byval n)
        i_expires = stdTime(DateAdd("s", n, Now)) '秒
    End Property

    Public Property Get Expires()
        [Expires] = i_expires
    End Property

	'@ *****************************************************************************************
	'@ 过程名:  Ctrl.Cache.Find(key) 缩写形式 Ctrl.Cache(key)
	'@ 返  回:  Anything (任意值)
	'@ 作  用:  获取缓存值
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'==DEMO=====================================================================================
	'@ ab.c.print Ctrl.Cache("ca_admin")
	'@ *****************************************************************************************

    Public Default Property Get Find(Byval key)
        Find = [Get](key)
    End Property

	'@ *****************************************************************************************
	'@ 过程名:  Ctrl.Cache.Get(key)
	'@ 返  回:  Anything (任意值)
	'@ 作  用:  取Cache值
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'==DEMO=====================================================================================
	'@ ab.c.print Ctrl.Cache.Get("ca_admin")
	'@ *****************************************************************************************

	Public Function [Get](Byval key)
        Dim iExpire : iExpire = Application(Me.Mark & key & "_Expires")
        If IsNull(iExpire) Or IsEmpty(iExpire) Then
            [Get] = ""
        Else
            If IsDate(iExpire) And CDate(iExpire) > Now Then
                [Get] = Application(Me.Mark & key)
            Else
                Call Remove(Me.Mark & key)
                [Get] = ""
            End If
        End If
    End Function

	'@ *****************************************************************************************
	'@ 过程名:  Ctrl.Cache.Set key, value, expire
	'@ 返  回:  无返回值
	'@ 作  用:  设置Session值
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'@ 参数 value: Anything (任意值)
	'@ 参数 expire: Interger Or "" (整数或)
	'==DEMO=====================================================================================
	'@ Ctrl.Cache.Set "ca_admin", "admin", "" '过期时间即为Ctrl.Cache.Expires属性值
	'@ Ctrl.Cache.Set "ca_admin", "admin", Now+1 '设置一天后过期
	'@ Ctrl.Cache.Set "ca_admin", "admin", DateAdd("s", 3600, Now) '设置3600秒后过期
	'@ *****************************************************************************************

	Public Sub [Set](Byval key, Byval value, Byval expire)
		On Error Resume Next
		Dim iexpire : iexpire = i_expires
        If Trim(expire&"")<>"" Then iexpire = expire
		iexpire = stdTime(Cdate(iexpire))
		If Err Or iexpire <= Now Then : Err.Clear : Exit Sub : End If
        Lock
		Application(Me.Mark & key) = Value
        Application(Me.Mark & key & "_Expires") = iexpire
        UnLock
		On Error Goto 0
    End Sub

	'@ *****************************************************************************************
	'@ 过程名:  Ctrl.Cache.UnLock
	'@ 返  回:  无返回值
	'@ 作  用:  Lock the applaction
	'==DESC=====================================================================================
	'@ 参数 : 无
	'==DEMO=====================================================================================
	'@ 无
	'@ *****************************************************************************************

	Public Sub Lock()
        Application.Lock()
    End Sub

	'@ *****************************************************************************************
	'@ 过程名:  Ctrl.Cache.UnLock
	'@ 返  回:  无返回值
	'@ 作  用:  UnLock the applaction
	'==DESC=====================================================================================
	'@ 参数 : 无
	'==DEMO=====================================================================================
	'@ 无
	'@ *****************************************************************************************

	Public Sub UnLock()
        Application.UnLock()
    End Sub

	'@ *****************************************************************************************
	'@ 过程名:  Ctrl.Cache.Remove key
	'@ 返  回:  无返回值
	'@ 作  用:  删除某个Session值
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'==DEMO=====================================================================================
	'@ Ctrl.Cache.Remove "ca_admin"
	'@ *****************************************************************************************

	Public Sub Remove(Byval key)
        Lock
        Application.Contents.Remove(Me.Mark & key)
        Application.Contents.Remove(Me.Mark & key & "_Expires")
        unLock
    End Sub

	'@ *****************************************************************************************
	'@ 过程名:  Ctrl.Cache.RemoveAll
	'@ 返  回:  无返回值
	'@ 作  用:  清空删除所有Cache值
	'==DESC=====================================================================================
	'@ 参数: 无
	'==DEMO=====================================================================================
	'@ Ctrl.Cache.RemoveAll()
	'@ *****************************************************************************************

	Public Sub RemoveAll()
        Lock
        Application.Contents.RemoveAll()
        unLock
    End Sub

    '**********
	'@ *****************************************************************************************
	'@ 过程名:  Ctrl.Cache.Compare(key1, key2)
	'@ 返  回:  True/False 布尔值
	'@ 作  用:  比较两个Cache值是否相等
	'==DESC=====================================================================================
	'@ 参数 key1: String (字符串) Cache 1 的键值
	'@ 参数 key2: String (字符串) Cache 2 的键值
	'==DEMO=====================================================================================
	'@ AB.C.Print Ctrl.Cache.Compare("s_v1", "s_v2")
	'@ *****************************************************************************************

	Public Function [Compare](Byval key1, Byval key2)
        Dim Cache1
        Cache1 = Me.[Get](key1)
        Dim Cache2
        Cache2 = Me.[Get](key2)
        If TypeName(Cache1) <> TypeName(Cache2) Then
            Compare = False
        Else
            If TypeName(Cache1) = "Object" Then
                Compare = (Cache1 Is Cache2)
            Else
                If TypeName(Cache1) = "Variant()" Then
                    Compare = (Join(Cache1, "^") = Join(Cache2, "^"))
                Else
                    Compare = (Cache1 = Cache2)
                End If
            End If
        End If
    End Function

	Private Function stdTime(Byval t)
		Dim tm : tm = t : If IsNull(t) Or t="" Then tm = Now
		Dim p : p = "yyyy-mm-dd hh:nn:ss"
		Dim s : s = p & ""
		s = Replace(s,"yyyy",year(tm))
		s = Replace(s,"mm",right("0" & month(tm),2))
		s = Replace(s,"dd",right("0" & day(tm),2))
		s = Replace(s,"hh",right("0" & hour(tm),2))
		s = Replace(s,"nn",right("0" & minute(tm),2))
		s = Replace(s,"ss",right("0" & second(tm),2))
		stdTime = s
	End Function

End Class
%>