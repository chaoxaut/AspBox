<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include virtual="/Inc/AspBox/Cls_AB.asp" -->
<%
Response.Charset = "UTF-8"
Response.CodePage = 65001

AB.Use "Rnd"
Dim rand
'rand = AB.Rnd.Rand(10000,99999)
rand = AB.C.Rand(10000,99999)
Dim url, rule, stamp
url = "http://open.baidu.com/special/time/?t=" & rand
rule = "window.baidu_time\((\d+)\);"

AB.C.WR "来源页地址：<a href='"& url &"' target=_blank>"& url &"</a>"
AB.Http.Get url
Dim Matches, Match
Set Matches = AB.C.RegMatch(AB.Http.Html,rule)
'If Matches.Count > 0 Then stamp = Matches(0).SubMatches(0)
For Each Match In Matches
stamp = Match.SubMatches(0)
Next
Set Matches = Nothing

'AB.C.WR stamp

Dim jsa(7)
jsa(0) = "var week = '日一二三四五六';"
jsa(1) = "var innerHtml = '{0}:{1}:{2}';"
jsa(2) = "var dateHtml = '{0}月{1}日 &nbsp;周{2}';"
jsa(3) = "var timer = 0;"
jsa(4) = "var beijingTimeZone = 8;"
jsa(5) = "function p(s){return s < 10 ? '0' + s : s;}"
jsa(6) = "" &_
"function format(str, json){" &_
"	return str.replace(/{(\d)}/g, function(a, key) {" &_
"		return json[key];" &_
"	});" &_
"}"
jsa(7) = "function stdTime(time){" & VBNewline &_
"	var timeOffset = ((-1 * (new Date()).getTimezoneOffset()) - (beijingTimeZone * 60)) * 60000;" & VBNewline &_
"	var now = new Date(time - timeOffset);" & VBNewline &_
"	var a = format(dateHtml, [ p((now.getMonth()+1)), p(now.getDate()), week.charAt(now.getDay())]);" & VBNewline &_
"	var b = format(innerHtml, [p(now.getHours()), p(now.getMinutes()), p(now.getSeconds())]);" & VBNewline &_
"	var tmp = a + ' '+ b;" & VBNewline &_
"	return tmp;" & VBNewline &_
"}"

Dim jstr
jstr = "" &_
"var pa = 'yyyy-MM-dd hh:mm:ss';" &_
"Date.prototype.getFormattedDate = function(pattern){" &_
"	function getFullStr(i){" &_
"		return i>9?''+i:'0'+i;" &_
"	}" &_
"	pattern = pattern.replace(/yyyy/,this.getFullYear())" &_
"		.replace(/MM/,getFullStr(this.getMonth()+1))" &_
"		.replace(/dd/,getFullStr(this.getDate()))" &_
"		.replace(/hh/,getFullStr(this.getHours()))" &_
"		.replace(/mm/,getFullStr(this.getMinutes()))" &_
"		.replace(/ss/,getFullStr(this.getSeconds()));" &_
"	return pattern;" &_
"};" &_
"var t1 = (new Date()).toString();" &_
"var t2 = (new Date()).getFormattedDate(pa);" &_
"var localTime = t2;"

'AB.Trace js
AB.Use "sc"
Dim sc : Set sc = ab.sc.new
sc.Lang = "js"
Dim i
For Each i In jsa
sc.Add i
Next
sc.Add jstr

AB.C.WR "<h2>北京时间 - 国家授时中心标准时间</h2>"
Dim jso : Set jso = sc.Object
dim localTime
localTime = jso.localTime
'AB.Use "Time"
'localTime = AB.Time.LocalTime

AB.C.WR "<span id=stdtime>" & jso.stdTime(stamp) & "</span>" '获取百度北京时间
AB.C.WR "<h3>时间校对</h3>"
AB.C.WR "您的本地电脑时间：<span id=localtime>" & localTime & "</span>" '服务器获取客户端(js)的本地时间
%>
<script>
var stamp = <%=stamp%>;
var week = '日一二三四五六';
var innerHtml = '{0}:{1}:{2}';
var dateHtml = '{0}月{1}日 &nbsp;周{2}';
var timer = 0;
var beijingTimeZone = 8;
function p(s){return s < 10 ? '0' + s : s;}
function format(str, json){
	return str.replace(/{(\d)}/g, function(a, key) {
		return json[key];
	});
}
function stdTime(time){
	var timeOffset = ((-1 * (new Date()).getTimezoneOffset()) - (beijingTimeZone * 60)) * 60000;
	var now = new Date(time - timeOffset);
	var a = format(dateHtml, [ p((now.getMonth()+1)), p(now.getDate()), week.charAt(now.getDay())]);
	var b = format(innerHtml, [p(now.getHours()), p(now.getMinutes()), p(now.getSeconds())]);
	var tmp = a + ' '+ b;
	return tmp;
}

/*百度北京时间显示*/
var showTime = function(time){	
	document.getElementById("stdtime").innerHTML = stdTime(time);
	timer = setInterval(function(){
		time += 1000;
		document.getElementById("stdtime").innerHTML = stdTime(time);
	}, 1000);
}
showTime(stamp);

/*本地电脑时间显示*/
var pa = 'yyyy-MM-dd hh:mm:ss';
Date.prototype.getFormattedDate = function(pattern){
	function getFullStr(i){
		return i>9?''+i:'0'+i;
	}
	pattern = pattern.replace(/yyyy/,this.getFullYear())
		.replace(/MM/,getFullStr(this.getMonth()+1))
		.replace(/dd/,getFullStr(this.getDate()))
		.replace(/hh/,getFullStr(this.getHours()))
		.replace(/mm/,getFullStr(this.getMinutes()))
		.replace(/ss/,getFullStr(this.getSeconds()));
	return pattern;
};
//document.getElementById("localtime").innerHTML = (new Date()).toString();
document.getElementById("localtime").innerHTML = (new Date()).getFormattedDate(pa);
setInterval(function(){
	document.getElementById("localtime").innerHTML = (new Date()).getFormattedDate(pa);
}, 1000);
</script>