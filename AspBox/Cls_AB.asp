<%
'######################################################################
'## Cls_AB.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox FrameWork
'## Version     :   v1.4.0Beta
'## Author      :   Lajox (lajox@19www.com)
'## Update Date :   2014/12/11 21:34
'## HomePage	:   http://www.19www.com
'## -------------------------------------------------------------------
'## Description :   AspBox For Asp FrameWork
'## -------------------------------------------------------------------
'## /*
'## Copyright (c) 2012, 19www.com - Lajox. All rights reserved.
'## For licensing, see LICENSE.html or http://www.19www.com/license
'## Thanks To: ColdStone Provide (EasyAsp) Initial Model
'## */
'######################################################################
%>
<!--#include file="AB.Config.asp"-->
<%
Class Cls_AB

	Public C,D,H,[Error],E,A,DB,Dbo,Fso,List,Json,Cache,Http,Xml,Tpl,X,Up,Upload,Form,[Char],Time,Html,[Rnd],Mail,[Cookie],[Session],Url,SC,jsLib,Pager
	Public Pub,Trace,Dict,DT,Config,Fn,[MVC],[Ace] 'MVC控制器
	Private s_path, s_plugin, s_library, s_cachePath, s_cores, s_nocores, s_ex
	Private s_fsoName, s_dictName, s_steamName, s_charset, s_bom, s_cacheType
	Private o_ext, o_lib, o_cache, o_dict, o_regex, o_fso, o_mvc, o_fn, o_cache_conn
	Private b_charset, b_debug, b_core, b_cooen, b_mvc, b_clearmark, b_iscached
	Private s_tbPrefix, s_tbSuffix '数据表前缀和后缀

	Private Sub Class_Initialize()
		s_ex 		= "ab"
		s_fsoName	= "Scripting.FileSystemObject"
		s_dictName	= "Scripting.Dictionary"
		s_steamName	= "ADODB.Stream"
		s_path		= "/Inc/AspBox/"
		s_library	= s_path & "library/"
		s_plugin	= s_path & "plugin/"
		s_cachePath	= s_path & "_Cache/"
		s_charset	= "UTF-8"
		s_bom		= "remove"
		b_core		= False '是否预加载核心
		b_charset	= False '是否设置了编码
		b_debug		= False '是否启用调试
		b_cooen		= False 'Cookie是否加密
		b_mvc		= False	'是否启用MVC模型
		b_clearmark = False	'是否清除Asp脚本标记
		s_cacheType = ""	'设置核心加载的缓存类型，False 或 空值 则表示禁用
		s_tbPrefix 	= "" 	'数据表前缀
		s_tbSuffix 	= "" 	'数据表后缀
		s_cores		= "C,D,H,[Error],E,A,DB,Dbo,Fso,List,Json,Cache,Http,Xml,Tpl,X,Up,Upload,Form,[Char],Time,Html,[Rnd],Mail,[Cookie],[Session],Url,SC,jsLib,Pager" '预定义核心
		s_nocores	= "DB,Dbo,SC,jsLib,Pager" '指定不加载的预定义核心
	End Sub

	Public Sub Init()
		On Error Resume Next
		Set Pub 	= New Cls_AB_Pub
		Set o_ext 	= Server.CreateObject(s_dictName)
		Set o_ext 	= Server.CreateObject(s_dictName)
		Set o_lib 	= Server.CreateObject(s_dictName)
		Set o_dict 	= Server.CreateObject(s_dictName)
		Set o_fn 	= Server.CreateObject(s_dictName)
		Set o_cache	= Server.CreateObject(s_dictName)
		Set o_regex = New Regexp
		o_regex.Global = True
		o_regex.IgnoreCase = True
		DictInit() '加载预定义字典库
		FixBaseCore() '修正预定义核心
		Core_Do "on", s_cores '加载预定义核心
		[Load] "C,D,H,[Error],E,JSON,X" '指定载入某些标识核心: [C:通用函数库、D:一般函数库、E:加密模块、X:额外拓展模块]
		[Error](0) = ""
		[Error](1) = "程序运行出错"
		[Error](2) = "包含文件内部运行错误，请检查包含文件代码！"
		[Error](3) = "读取文件错误，文件未找到！"
		[Error](4) = "系统路径错误，请检查相关设置！"
		Set Trace = Ext("Trace")
		Mvc_Do() '加载MVC控制器
		FnInit()
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		[Exit]()
	End Sub

	Public Sub [Exit]()
		On Error Resume Next
		Core_Do "off", s_cores
		[Drop] "H,C,D,E,JSON,X,Dbo,DB"
		[Drop] "[Error],[MVC],[Ace]"
		ClearExt()
		ClearLib()
		Free "o_ext,o_lib,o_cache,o_dict,o_regex,o_fso,o_mvc,o_fn,Dict,DT,Config,Fn"
		Execute "Set Trace = Nothing"
		Execute "o_cache_conn.Close : Set o_cache_conn = Nothing"
		ExecuteGlobal "Set AB = Nothing"
		If IsObject(Abx_o_ctrl) Then Set Abx_o_ctrl = Nothing
		On Error GoTo 0
	End Sub

	Public Sub Quit()
		[Exit]()
	End Sub

	Public Property Get SoftName()
		SoftName = "AspBox"
	End Property
	Public Property Get Version()
		Version = "v1.4.0 Build 20141211"
	End Property
	Public Property Get RegID()
		RegID = "1.40.141211.00"
	End Property

	Public Property Let [CharSet](ByVal s)
		s_charset = Ucase(s)
		b_charset = True
	End Property
	Public Property Get [CharSet]()
		[CharSet] = s_charset
	End Property

	Public Property Let ifCharSet(ByVal s)
		b_charset = s
	End Property
	Public Property Get ifCharSet()
		ifCharSet = b_charset
	End Property

	Public Property Let ifCore(ByVal s)
		b_core = s
	End Property
	Public Property Get ifCore()
		ifCore = b_core
	End Property

	Public Property Let ifMVC(ByVal s)
		b_mvc = s
	End Property
	Public Property Get ifMVC()
		ifMVC = b_mvc
	End Property

	Public Property Let baseCore(ByVal p)
		s_cores = p
	End Property
	Public Property Get baseCore()
		baseCore = s_cores
	End Property

	Public Property Let noCore(ByVal p)
		s_nocores = p
	End Property
	Public Property Get noCore()
		noCore = s_nocores
	End Property

	Public Property Let basePath(ByVal p)
		s_path = AB_FixAbsPath(p)
		s_plugin = s_path & "plugin/"
		s_library = s_path & "library/"
		s_cachePath	= s_path & "_Cache/"
	End Property
	Public Property Get basePath()
		basePath = s_path
	End Property

	Public Property Let pluginPath(ByVal p)
		s_plugin = AB_FixAbsPath(p)
	End Property
	Public Property Get pluginPath()
		pluginPath = s_plugin
	End Property

	Public Property Let CorePath(ByVal p)
		s_library = AB_FixAbsPath(p)
	End Property
	Public Property Get CorePath()
		CorePath = s_library
	End Property

	Public Property Let CachePath(ByVal p)
		s_cachePath = AB_FixAbsPath(p)
	End Property
	Public Property Get CachePath()
		CachePath = s_cachePath
	End Property

	Public Property Let FileBOM(ByVal s)
		s_bom = Lcase(s)
	End Property
	Public Property Get FileBOM()
		FileBOM = s_bom
	End Property

	Public Property Let ifClearMark(ByVal s)
		b_clearmark = s
	End Property
	Public Property Get ifClearMark()
		ifClearMark = b_clearmark
	End Property

	Public Property Let tbPrefix(ByVal s)
		s_tbPrefix = s
	End Property
	Public Property Get tbPrefix()
		tbPrefix = s_tbPrefix
	End Property

	Public Property Let tbSuffix(ByVal s)
		s_tbSuffix = s
	End Property
	Public Property Get tbSuffix()
		tbSuffix = s_tbSuffix
	End Property

	Public Property Let fsoName(ByVal s)
		s_fsoName = s
	End Property
	Public Property Get fsoName()
		fsoName = s_fsoName
	End Property

	Public Property Let steamName(ByVal s)
		s_steamName = s
	End Property
	Public Property Get steamName()
		steamName = s_steamName
	End Property

	Public Property Let dictName(ByVal s)
		s_dictName = s
	End Property
	Public Property Get dictName()
		dictName = s_dictName
	End Property

	Public Property Let [Debug](ByVal b)
		On Error Resume Next
		b_debug = b
		IF IsObject([Error]) Then
			[Error].debug = b
			IF Err.Number<>0 Then Err.Clear
		End IF
		On Error Goto 0
	End Property
	Public Property Get [Debug]
		[Debug] = b_debug
	End Property

	Public Property Let CookieEncode(ByVal b)
		On Error Resume Next
		b_cooen = b
		If HasCore("Cookie") Then Cookie.IfEncode = b
		On Error Goto 0
	End Property
	Public Property Get CookieEncode()
		CookieEncode = b_cooen
	End Property

	Public Property Let CacheType(ByVal s)
		s_cacheType = s
		Select Case LCase(s_cacheType)
			Case "app","true","1" : s_cacheType = "app"
			Case "file","2" : s_cacheType = "file"
			Case "data","db","database","3" : s_cacheType = "data"
			Case "false","","0","null","empty",null : s_cacheType = ""
			Case Else : s_cacheType = ""
		End Select
	End Property
	Public Property Get CacheType()
		Select Case LCase(s_cacheType)
			Case "app","true","1" : s_cacheType = "app"
			Case "file","2" : s_cacheType = "file"
			Case "data","db","database","3" : s_cacheType = "data"
			Case "false","","0","null","empty",null : s_cacheType = ""
			Case Else : s_cacheType = ""
		End Select
		CacheType = s_cacheType
	End Property

	Public Property Get ScriptTime
		ScriptTime = Pub.toNumber(Pub.GetScriptTime(0)/1000,3)
	End Property

	Public Property Get dbQueryTimes
		dbQueryTimes = Abx_DbQueryTimes
	End Property
	Public Property Get QT
		QT = Abx_DbQueryTimes
	End Property

	Public Sub InitQT()
		Abx_DbQueryTimes = 0
	End Sub

	Public Sub CacheClean()
		On Error Resume Next
		Dim o_fso, f, fd : Set o_fso = Server.CreateObject(s_fsoName)
		Dim path : path = s_cachePath
		If Mid(path,2,1)<>":" Then path = Server.MapPath(path)
		Set fd = o_fso.GetFolder(path)
		For Each fs in fd.SubFolders
			If o_fso.FolderExists(fs) Then o_fso.DeleteFolder fs
		Next
		For Each fs in fd.Files
			If o_fso.FileExists(fs) Then o_fso.DeleteFile fs
		Next
		Set fd = Nothing
		Set o_fso = Nothing
		On Error Goto 0
	End Sub

	Public Function HasCore(ByVal s)
		On Error Resume Next
		Dim b : b = False
		Dim f : f = s
		f = Lcase(Pub.RegReplace(f,"^\[(.*)\]$","$1"))
		If f = "ace" Then f = "mvc"
		If LCase(TypeName(Eval("["&f&"]"))) = "cls_ab_" & f Then
			b = True
		End IF
		HasCore = b
		On Error Goto 0
	End Function

	Public Sub addCore(ByVal s)
		s_cores = AB_appendCore(s)
	End Sub

	Public Sub FixBaseCore()
		AB_FixBaseCore()
	End Sub

	'----字典库操作块 b----

	Private Sub DictInit()
		o_dict.add "dict","Scripting.Dictionary" '(内置 字典对象)
		o_dict.add "fso","Scripting.FileSystemObject" '(内置 文件系统组件)
		o_dict.add "stream","ADODB.Stream" '(ADO 数据流对象, 常在无组件上传中用到)
		o_dict.add "conn","ADODB.Connection" '(ADO 数据库链接对象)
		o_dict.add "rs","ADODB.RecordSet" '(数据库记录集操作对象)
		o_dict.add "cmd","ADODB.Command" '(CMD对象, 长用在存储过程)
		o_dict.add "xmlhttp","Microsoft.XMLHTTP" '(Http 组件, 常在采集系统中用到)
		o_dict.add "xml2http","MSXML2.XMLHTTP"
		o_dict.add "serxmlhttp","MSXML2.ServerXMLHTTP"
		o_dict.add "serxmlhttp3","Msxml2.ServerXMLHTTP.3.0"
		o_dict.add "sc","ScriptControl" '内置 Microsoft ScriptControl 控件
		o_dict.add "mssc","MSScriptControl.ScriptControl" '内置 ScriptControl 控件
		o_dict.add "shell","WScript.Shell" '(Shell 组件, 可能涉及安全问题)
		o_dict.add "shellapp","Shell.Application" '(Shell Application)
		o_dict.add "guid","Scriptlet.TypeLib"
		o_dict.add "browser","MSWC.BrowserType"
		o_dict.add "nextlink","MSWC.NextLink"
		o_dict.add "tools","MSWC.Tools"
		o_dict.add "status","MSWC.Status"
		o_dict.add "counters","MSWC.Counters"
		o_dict.add "adrotator","MSWC.AdRotator"
		o_dict.add "permisscheck","MSWC.PermissionChecker"
		o_dict.add "aspjpeg","Persits.Jpeg" '(ASPjpeg 图片处理组件)
		o_dict.add "aspemail","Persits.MailSender" '(ASPemail 发信)
		o_dict.add "jmail","JMail.Message" '(JMail邮件收发, JMail4.3以上版本才改过来的对象; 为了兼容, JMail.SMTPmail也可使用)
		o_dict.add "jmailsmtp","JMail.SmtpMail" '(Dimac 公司的 W3 Jmail 邮件收发, 4.3以前版本)
		o_dict.add "cdonts","CDONTS.NewMail" '(CDONTS邮件收发)
		o_dict.add "cdomail","CDO.Message" '(CDOSYS, 内置 Window 2003 New SendMailCom Object)
		o_dict.add "dkqmail","DkQmail.Qmail" '(dkQmail 发信)
		o_dict.add "aspmail","SMTPsvg.Mailer" '(ASPmail 发信)
		o_dict.add "smtpmail","SmtpMail.SmtpMail.1" '(SmtpMail 发信)
		o_dict.add "aspupload","Persits.Upload" '(ASPUpload 文件上传)
		o_dict.add "aspsmartupload","ASPSmartUpload.SmartUpload"
		o_dict.add "lyfupload","LyfUpload.UploadFile" '(刘云峰的文件上传组件)
		o_dict.add "ironupload","IronSoft.Upload" '(国产免费, 上传组件)
		o_dict.add "dvupload","DvFile.Upload"
		o_dict.add "w3upload","w3.Upload" '(Dimac 文件上传)
		o_dict.add "sa-fileup","SoftArtisans.FileUp" '(SA-FileUp 文件上传)
		o_dict.add "sa-fm","SoftArtisans.FileManager" '(SoftArtisans 文件管理)
		o_dict.add "sa-imgwriter","SoftArtisans.ImageGen" '(SA 的图像读写组件)
		o_dict.add "aspuploadprogress","Persits.UploadProgress" '(AspUpload 组件 上传进度对象)
		o_dict.add "cdoconfig","CDO.Configuration" '(内置 CDO的Configuration对象)
		o_dict.add "w3image","W3Image.Image" '(Dimac 的图像读写组件)
		o_dict.add "xygraph","XY.Graphics" '(国产免费, 图像/图表处理)
		o_dict.add "irondrawpic","Ironsoft.DrawPic" '(国产免费, 图像/图形处理)
		o_dict.add "ironflashcap","Ironsoft.FlashCapture" '(国产免费, 多功能 FLASH 截图)
		o_dict.add "dyyiis","hin2.com_iis" '(国产免费, 呆呆IIS管理组件)
		o_dict.add "socket","Socket.TCP" '(Dimac 公司的 Socket 组件)
		o_dict.add "jetengine","JRO.JetEngIne"
		o_dict.add "rotator","IISSample.ContentRotator"
		o_dict.add "pagecounter","IISSample.PageCounter"
		Set Dict = o_dict
		Set DT = o_dict
		Set Config = o_dict
	End Sub

	Public Sub DictAdd(ByVal k, ByVal v) '配置额外字典库
		If Not o_dict.Exists(k) Then
			o_dict.add k, v
		Else
			If IsObject(v) Then
				Set o_dict(k) = v
			Else
				If IsObject(o_dict(k)) Then o_dict(k) = Empty
				o_dict(k) = v
			End If
		End IF
		Set Dict = o_dict
		Set DT = o_dict 'AB.Dict的缩写形式AB.DT
		Set Config = o_dict
	End Sub
	'Call AB.DictAdd(key, value) => Call AB.DA(key, value)
	Public Sub Da(ByVal k, ByVal v):DictAdd k, v:End Sub

	Public Sub DictDel(ByVal k) '移除额外字典库某项
		If o_dict.Exists(k) Then
			o_dict.Remove(k)
		End IF
		Set Dict = o_dict
		Set DT = o_dict
		Set Config = o_dict
	End Sub
	'Call AB.DictDel(key) => Call AB.DD(key)
	Public Sub DD(ByVal k):DictDel(k):End Sub

	Public Sub DictClear() '清空所有额外字典库
		o_dict.RemoveAll
		Set Dict = o_dict
		Set DT = o_dict
		Set Config = o_dict
	End Sub
	'Call AB.DictClear() => Call AB.Dc()
	Public Sub Dc():DictClear():End Sub

	'----字典库操作块 e----

	Private Sub ClearLib()
		Dim i
		If Pub.Has(o_lib) Then
			For Each i In o_lib
				Set o_lib(i) = Nothing
			Next
			o_lib.RemoveAll
		End If
	End Sub

	Private Sub ClearExt()
		Dim i
		If Pub.Has(o_ext) Then
			For Each i In o_ext
				Set o_ext(i) = Nothing
			Next
			o_ext.RemoveAll
		End If
	End Sub

	Private Sub Core_Do(ByVal t, ByVal s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim p, f, g
		If Not Pub.isFolder(s_library) Then
			Me.ShowErr 0, "程序运行出错, AspBox核心文件引入失败，请检查并修改配置文件。"
		End If
		Select Case t
			Case "on"
				IF b_core Then
					For i = 0 To Ubound(a_core)
						g = Pub.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
						f = Lcase(g)
						If f="ace" Then f="mvc"
						p = s_library & "ab." & f & ".asp"
						'If Not Pub.IsLoaded_Core(s_ex, f, o_lib) Then:Execute "Set [" & g & "] = New Cls_AB_Obj":End IF
						If Not Pub.IsLoaded_Core(s_ex, f, o_lib) Then
							If Pub.isFile(p) Then
								If (f <> "mvc") Or (f = "mvc" and b_mvc = True) Then
									Pub.CoreInclude s_ex, f, o_lib, p
									If f = "mvc" Or f = "ace" Then
										If (Not o_lib.Exists("mvc")) Or Lcase(o_lib("mvc")) <> "cls_ab_mvc" Then
											Execute("Set o_lib(""mvc"") = New Cls_AB_Mvc")
										End If
										If o_lib.Exists("mvc") and Lcase(TypeName(o_lib("mvc"))) = "cls_ab_mvc" Then Execute "Set o_mvc = o_lib(""mvc"")"
										If LCase(TypeName(o_mvc))="cls_ab_mvc" Then
											Execute "Set [Mvc] = o_mvc"
											Execute "Set [Ace] = o_mvc"
											ExecuteGlobal "Dim [Mvc] : Set [Mvc] = AB.Mvc.Ace"
											ExecuteGlobal "Dim [Ace] : Set [Ace] = [Mvc]"
											Execute "Set o_mvc = Nothing"
										End If
									Else
										Execute("Set o_lib(""" & f & """) = New Cls_AB_" & g)
										If Lcase(o_lib(""& f)) = "cls_ab_"& f Then Execute "Set [" & g & "] = o_lib(""" & f & """)"
									End If
								End If
							End If
						End If
					Next
				End IF
			Case "off"
				For i = Ubound(a_core) To 0 Step -1
					a_core(i) = Pub.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
					Execute "Set [" & a_core(i) & "] = Nothing"
					If o_lib.Exists(Lcase(a_core(i))) Then Execute "Set [" & o_lib(Lcase(a_core(i))) & "] = Nothing"
					o_lib.Remove(Lcase(a_core(i)))
				Next
		End Select
		On Error Goto 0
	End Sub

	Private Sub Mvc_Do()
		On Error Resume Next
		Dim f, p : p = s_library & "ab.mvc.asp"
		'If LCase(TypeName(AB.Mvc)) <> "cls_ab_mvc" Then:Execute "Set [Mvc] = Cls_AB_Obj":End IF
		IF b_mvc Then
			f = "mvc"
			If Not Pub.IsLoaded_Core(s_ex, f, o_lib) Then
				If Pub.isFile(p) Then
					AB_xInclude p,"mvc"
					If (Not o_lib.Exists("mvc")) Or Lcase(o_lib("mvc")) <> "cls_ab_mvc" Then
						Execute("Set o_lib(""mvc"") = (New Cls_AB_Mvc)")
					End If
					If o_lib.Exists("mvc") and Lcase(TypeName(o_lib("mvc"))) = "cls_ab_mvc" Then Execute "Set o_mvc = o_lib(""mvc"")"
					If LCase(TypeName(o_mvc))="cls_ab_mvc" Then
						Execute "Set [Mvc] = o_mvc"
						Execute "Set [Ace] = o_mvc"
						ExecuteGlobal "Dim [Mvc] : Set [Mvc] = AB.Mvc.Ace"
						ExecuteGlobal "Dim [Ace] : Set [Ace] = [Mvc]"
						Execute "Set o_mvc = Nothing"
					End If
				End If
			End If
		End IF
		On Error Goto 0
	End Sub

	Public Sub [Load](Byval s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim p, f, g
		For i = 0 To Ubound(a_core)
			If trim(a_core(i))<>"" Then
				g = Pub.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
				f = Lcase(g)
				If f="ace" Then f="mvc"
				p = s_library & "ab." & f & ".asp"
				If Not Pub.IsLoaded_Core(s_ex, f, o_lib) Then
					If Pub.isFile(p) Then
						If (f <> "mvc") Or (f = "mvc" and b_mvc = True) Then
							Pub.CoreInclude s_ex, f, o_lib, p
							If f = "mvc" Or f = "ace" Then
								If (Not o_lib.Exists("mvc")) Or Lcase(o_lib("mvc")) <> "cls_ab_mvc" Then
									Execute("Set o_lib(""mvc"") = New Cls_AB_Mvc")
								End If
								If o_lib.Exists("mvc") and Lcase(TypeName(o_lib("mvc"))) = "cls_ab_mvc" Then Execute "Set o_mvc = o_lib(""mvc"")"
								If LCase(TypeName(o_mvc))="cls_ab_mvc" Then
									Execute "Set [Mvc] = o_mvc"
									Execute "Set [Ace] = o_mvc"
									ExecuteGlobal "Dim [Mvc] : Set [Mvc] = AB.Mvc.Ace"
									ExecuteGlobal "Dim [Ace] : Set [Ace] = [Mvc]"
									Execute "Set o_mvc = Nothing"
								End If
							Else
								Execute("Set o_lib(""" & f & """) = New Cls_AB_" & g)
								If Lcase(o_lib(""& f)) = "cls_ab_"& f Then Execute "Set [" & g & "] = o_lib(""" & f & """)"
							End If
						End If
					End If
				End If
			End If
		Next
		On Error Goto 0
	End Sub

	Public Sub ReLoad(ByVal s)
		[Drop] s
		[Load] s
	End Sub

	Public Default Function [Lib](ByVal o)
		On Error Resume Next
		If isnull(o) or trim(o)="" Then : [Lib] = Empty : Exit Function : End If
		Dim loaded : loaded = False
		Dim p, f, g
		g = Pub.RegReplace(Trim(o),"^\[(.*)\]$","$1")
		f = Lcase(g)
		If f="ace" Then f="mvc"
		p = s_library & "ab." & f & ".asp"
		If o_lib.Exists(""& f) and Lcase(TypeName(o_lib(""& f))) = "cls_ab_"& f Then
			loaded = True
			Set [Lib] = o_lib(""& f)
			Exit Function
		ElseIf Lcase(TypeName("AB.[" & g & "]")) = "cls_ab_"& f Then
			loaded = True
			'Execute("Set o_lib(""" & f & """) = New Cls_AB_" & f)
			Execute("Set o_lib(""" & f & """) = AB.[" & g & "]")
			Set [Lib] = o_lib(f)
			Exit Function
		Else
			loaded = False
			If Pub.isFile(p) Then
				Pub.CoreInclude s_ex, f, o_lib, p
				Execute("Set o_lib(""" & f & """) = New Cls_AB_" & f)
			Else
				AB.setError "(File Not Found: """ & p & """)", 4
			End If
		End If
		IF IsObject(o_lib(f)) Then Set [Lib] = o_lib(f)
		IF Err.Number<>0 Or Not IsObject(o_lib(f)) Then : [Lib] = Empty : Err.Clear : End IF
		On Error Goto 0
	End Function

	Public Sub Use(ByVal s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim p, f, g, t
		For i = 0 To Ubound(a_core)
			If trim(a_core(i))<>"" Then
				g = Pub.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
				f = Lcase(g)
				If f="ace" Then f="mvc"
				p = s_library & "ab." & f & ".asp"
				IF Not Pub.IsLoaded_Core(s_ex, f, o_lib) Then
					t = Eval("LCase(TypeName([" & g & "]))")
					If t = "cls_ab_obj" Or t = "nothing" Or t = "empty" Or t = "null" Then
						If Pub.isFile(p) Then
							'If (f <> "mvc") Or (f = "mvc" and b_mvc = True) Then
								Pub.CoreInclude s_ex, f, o_lib, p
								If f = "mvc" Or f = "ace" Then
									If (Not o_lib.Exists("mvc")) Or Lcase(o_lib("mvc")) <> "cls_ab_mvc" Then
										Execute("Set o_lib(""mvc"") = New Cls_AB_Mvc")
									End If
									If o_lib.Exists("mvc") and Lcase(TypeName(o_lib("mvc"))) = "cls_ab_mvc" Then Execute "Set o_mvc = o_lib(""mvc"")"
									If LCase(TypeName(o_mvc))="cls_ab_mvc" Then
										Execute "Set [Mvc] = o_mvc"
										Execute "Set [Ace] = o_mvc"
										ExecuteGlobal "Dim [Mvc] : Set [Mvc] = AB.Mvc.Ace"
										ExecuteGlobal "Dim [Ace] : Set [Ace] = [Mvc]"
										Execute "Set o_mvc = Nothing"
									End If
								Else
									Execute("Set o_lib(""" & f & """) = New Cls_AB_" & g)
									If Lcase(o_lib(""& f)) = "cls_ab_"& f Then Execute "Set [" & g & "] = o_lib(""" & f & """)"
								End If
								If f = "mvc" Then b_mvc = True
							'End If
						Else
							AB.setError "(File Not Found: """ & p & """)", 4
						End If
					End If
				End IF
			End If
		Next
		On Error Goto 0
	End Sub

	Public Sub ReUse(ByVal s)
		[Drop] s
		[Use] s
	End Sub

	Public Function Ext(ByVal o)
		On Error Resume Next
		If isnull(o) or trim(o)="" Then : Ext = Empty : Exit Function : End If
		Dim loaded : loaded = False
		Dim p, f, g
		g = Pub.RegReplace(Trim(o),"^\[(.*)\]$","$1")
		f = Lcase(g)
		p = s_plugin & "ab." & f & ".asp"
		If o_ext.Exists(""& f) and Lcase(TypeName(o_ext(""& f))) = "cls_ab_"& f Then
			loaded = True
			Set Ext = o_ext(""& f)
			Exit Function
		Else
			loaded = False
			If Pub.isFile(p) Then
				Pub.CoreInclude s_ex, f, o_lib, p
				Execute("Set o_ext(""" & f & """) = New Cls_AB_" & f)
			Else
				AB.setError "(File Not Found: """ & p & """)", 4
			End If
		End If
		IF IsObject(o_ext(f)) Then Set Ext = o_ext(f)
		IF Err.Number<>0 Or Not IsObject(o_ext(f)) Then : Ext = Empty : Err.Clear : End IF
		On Error Goto 0
	End Function

	Public Sub [Drop](Byval s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim f, g
		For i = 0 To Ubound(a_core)
			If trim(a_core(i))<>"" Then
				g = Pub.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
				f = Lcase(g)
				If f="ace" Then f="mvc"
				If f = "mvc" Or f = "ace" Then
					If IsObject(Mvc) Or IsObject(Ace) Then
						Execute "Set [Mvc] = Nothing"
						Execute "Set [Ace] = Nothing"
						ExecuteGlobal "Dim [Mvc] : Set [Mvc] = Nothing"
						ExecuteGlobal "Dim [Ace] : Set [Ace] = Nothing"
					End If
					If IsObject(o_lib) and Lcase(TypeName(o_lib))="dictionary" Then
						If o_lib.Exists("mvc") Then
							If IsObject(o_lib("mvc")) Then Execute("Set o_lib(""mvc"") = Nothing")
							o_lib.Remove("mvc")
						End If
					End If
				Else
					If IsObject(o_lib) and Lcase(TypeName(o_lib))="dictionary" Then
						If o_lib.Exists(""&f) Then
							If IsObject(o_lib(""&f)) Then Execute("Set o_lib(""" & f & """) = Nothing")
							o_lib.Remove(""&f)
						End If
					End If
					If f="db" Then
						If Lcase(TypeName(db))="cls_ab_db" Then
							If IsObject(db.Conn) And TypeName(db.Conn) = "Connection" Then
								If db.Conn.State = 1 Then db.Conn.Close()
							End If
						End If
					End If
					Execute "Set [" & g & "] = Nothing"
				End If
			End If
		Next
		On Error Goto 0
	End Sub

	Public Function Free(ByRef o)
		On Error Resume Next
		IF IsNull(o) Or IsEmpty(o) Then Exit Function
		Dim i,j,arr,str
		IF VarType(o)=8 Then '字符串
			IF Trim(o)="" Then Exit Function
			arr = Split(o, ",")
			IF UBound(arr)=0 Then '销毁单一对象
				IF Trim(arr(0))<>"" Then
					str = Eval("LCase(TypeName(arr(0)))")
					If str="connection" Then : Execute("If arr(0).State=1 Then arr(0).Close()") : End If
					If str="recordset" Then : Execute("If arr(0).State=1 Then arr(0).Close()") : End If
					Execute("Set "&arr(0)&" = Nothing")
				End If
			Else '销毁多个对象
				For i = 0 To UBound(arr)
					IF Trim(arr(i))<>"" Then
						str = Eval("LCase(TypeName(arr(i)))")
						If str="connection" Then : Execute("If arr(i).State=1 Then arr(i).Close()") : End If
						If str="recordset" Then : Execute("If arr(i).State=1 Then arr(i).Close()") : End If
						If str="dictionary" Then '字典对象
							If Has(Eval("arr(i)")) Then
								Execute("For Each j In arr(i) : Set arr(i)(j) = Nothing : Next : arr(i).RemoveAll")
							End If
						End If
						Execute("Set "&arr(i)&" = Nothing")
					End If
				Next
			End IF
		ElseIF IsObject(o) Then
			str = LCase(TypeName(o))
			If str="connection" Then : If o.State=1 Then o.Close() : End If
			If str="recordset" Then : If o.State=1 Then o.Close() : End If
			If str="dictionary" Then '字典对象
				If Pub.Has(o) Then
					For Each j In o : Set o(j) = Nothing : Next : o.RemoveAll
				End If
			End If
			Set o = Nothing
		ElseIf IsArray(o) Then
			If UBound(o)=-1 Then Exit Function
			For Each i In o
				If VarType(i)=8 Or IsObject(i) Then
					Me.Free(i)
				End If
			Next
		End IF
		On Error Goto 0
	End Function

	Public Sub TraceAll(ByVal o)
		Ext("Trace").TraceAll(o)
	End Sub

	'-------------------------------------------------------------------------------------------
	'# AB.FnInit()
	'# @return: void
	'# @dowhat: 重置AB.Fn(一旦重置则以前增设的所有属性将全部失效)
	'--DESC-------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO-------------------------------------------------------------------------------------
	'# none
	'-------------------------------------------------------------------------------------------

	Public Sub FnInit()
		On Error Resume Next
		Dim i
		If Pub.Has(o_fn) Then
			For Each i In o_fn
				Set o_fn(i) = Nothing
			Next
			o_fn.RemoveAll
		End If
		Execute("Set Fn = Nothing")
		On Error Goto 0
	End Sub

	'-------------------------------------------------------------------------------------------
	'# AB.FnAdd(str, obj)
	'# @alias: AB.FnSet(str, obj)
	'# @return: void
	'# @dowhat: 给AB.Fn拓展增加属性值(可以是 对象 或 字符串)
	'--DESC-------------------------------------------------------------------------------------
	'# @param str   [string] : 字符串  即作为AB.Fn的变量属性名
	'# @param obj   [object or string] : 对象或字符串  给该属性赋值为对象或字符串
	'--DEMO-------------------------------------------------------------------------------------
	'# class cls_t: function foo() : foo = "hello!" : end function : end class
	'# class cls_a: function sum(a,b) : sum = a + b : end function : end class
	'# class cls_b: sub sayhello() : response.write("hi,jack") : end sub : end class
	'# dim o_t : set o_t = New cls_t
	'# dim o_a : set o_a = New cls_a
	'# dim o_b : set o_b = New cls_b
	'# ab.fnAdd "m", o_t
	'# ab.fnAdd "n", o_a
	'# AB.C.PrintCn ab.fn.m.foo() '输出：hello!
	'# AB.C.PrintCn ab.fn.n.sum(2,5) '输出：7
	'# AB.C.PrintCn ab.fnHas("m") '输出：True
	'# AB.C.PrintCn ab.fnHas("abc") '输出：False
	'# AB.Trace ab.fnItems '输出全部定义属性值
	'# AB.Trace ab.fnItem("m") '输出属性 m 的值
	'# ab.fnAdd "m", o_b '此属性 m 的值被覆盖，值变为 o_b 对象
	'# ab.fn.m.sayhello() '输出：hi,jack
	'# ab.fnAdd "n", "abcdefg" '此属性 n 的值被覆盖，值变为字符串 abcdefg
	'# AB.Trace ab.fnItem("n")
	'# ab.fnAdd "n", o_t '此属性 n 的值再次被覆盖，值又变为 o_t 对象
	'# AB.Trace ab.fn.n
	'-------------------------------------------------------------------------------------------

	Public Sub FnAdd(ByVal s, ByVal o)
		On Error Resume Next
		s = Trim(s)
		If Not FnHas(s) Then
			o_fn.add LCase(s), o
		Else
			If IsObject(o) Then
				Set o_fn(s) = o
			Else
				If IsObject(o_fn(s)) Then o_fn(s) = Empty
				o_fn(s) = o
			End If
		End IF
		Dim i, strCode, temp, p : p = 0
		'Set Abx_o_ctrl = CreateObject("ScriptControl")
		Set Abx_o_ctrl = Server.CreateObject("MSScriptControl.ScriptControl")
		Abx_o_ctrl.Language = "VBScript"
		For Each i In o_fn
			If IsObject(o_fn(i)) Then
				Abx_o_ctrl.addObject i&"", o_fn(i), True
			Else
				If p = 1 Then
					strCode = o_fn(i)
				Else
					temp = o_fn(i)
					If VarType(o_fn(i)) = vbString Then temp = """" & o_fn(i) & """"
					strCode = "Function "&i&"(): "&i&" = "&temp&" : End Function"
				End If
				'Abx_o_ctrl.Modules("Global").ExecuteStatement strCode
				Abx_o_ctrl.addCode strCode
			End If
		Next
		'Set Fn = Abx_o_ctrl.Modules("Global").CodeObject
		Set Fn = Abx_o_ctrl.CodeObject
		On Error Goto 0
	End Sub
	Public Sub FnSet(ByVal s, ByVal o): FnAdd s, o : End Sub

	'-------------------------------------------------------------------------------------------
	'# AB.FnDel(s)
	'# @return: void
	'# @dowhat: 删除AB.Fn拓展的某个属性
	'--DESC-------------------------------------------------------------------------------------
	'# @param s   [string] : 字符串  要删除的AB.Fn的属性变量名
	'--DEMO-------------------------------------------------------------------------------------
	'# class cls_t: function foo() : foo = "hello!" : end function : end class
	'# class cls_a: function sum(a,b) : sum = a + b : end function : end class
	'# dim o_t : set o_t = New cls_t
	'# dim o_a : set o_a = New cls_a
	'# ab.fnAdd "m", o_t
	'# ab.fnAdd "n", o_a
	'# AB.C.PrintCn ab.fnHas("m") '输出：True
	'# If ab.fnHas("m") Then AB.C.PrintCn ab.fn.m.foo()
	'# ab.FnDel "m" '删除属性m
	'# AB.C.PrintCn ab.fnHas("m") '输出：False
	'-------------------------------------------------------------------------------------------

	Public Sub FnDel(ByVal s)
		On Error Resume Next
		s = Trim(s)
		If FnHas(s) Then
			'If IsObject(o_fn(s)) Then o_fn(s) = Empty
			o_fn.Remove(s)
		End IF
		Dim i, strCode, temp, p : p = 0
		'Set Abx_o_ctrl = CreateObject("ScriptControl")
		Set Abx_o_ctrl = Server.CreateObject("MSScriptControl.ScriptControl")
		Abx_o_ctrl.Language = "VBScript"
		For Each i In o_fn
			If IsObject(o_fn(i)) Then
				Abx_o_ctrl.addObject i&"", o_fn(i), True
			Else
				If p = 1 Then
					strCode = o_fn(i)
				Else
					temp = o_fn(i)
					If VarType(o_fn(i)) = vbString Then temp = """" & o_fn(i) & """"
					strCode = "Function "&i&"(): "&i&" = "&temp&" : End Function"
				End If
				'Abx_o_ctrl.Modules("Global").ExecuteStatement strCode
				Abx_o_ctrl.addCode strCode
			End If
		Next
		'Set Fn = Abx_o_ctrl.Modules("Global").CodeObject
		Set Fn = Abx_o_ctrl.CodeObject
		On Error Goto 0
	End Sub

	'-------------------------------------------------------------------------------------------
	'# AB.FnHas(str)
	'# @return: Boolean (布尔值)
	'# @dowhat: 检测AB.Fn拓展是否含有某项属性
	'--DESC-------------------------------------------------------------------------------------
	'# @param str   [string] : 字符串 即作为AB.Fn的变量属性名
	'--DEMO-------------------------------------------------------------------------------------
	'# class cls_t: function foo() : foo = "hello!" : end function : end class
	'# dim o_t : set o_t = New cls_t
	'# ab.fnAdd "m", o_t
	'# AB.C.PrintCn ab.fnHas("m") '输出：True
	'# AB.C.PrintCn ab.fnHas("abc") '输出：False
	'-------------------------------------------------------------------------------------------

	Public Function FnHas(ByVal s)
		On Error Resume Next
		FnHas = False
		If o_fn.Exists(s) Then FnHas = True
		On Error Goto 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.FnItem(str)
	'# @alias: AB.FnAttr(str)
	'# @return: Anything
	'# @dowhat: 获取AB.Fn拓展的某属性值
	'--DESC-------------------------------------------------------------------------------------
	'# @param str   [string] : 字符串 即作为AB.Fn的变量属性名
	'--DEMO-------------------------------------------------------------------------------------
	'# class cls_t: function foo() : foo = "hello!" : end function : end class
	'# dim o_t : set o_t = New cls_t
	'# ab.fnAdd "m", o_t
	'# AB.Trace ab.fn.m '输出 ab.fn.m 的值
	'# AB.Trace ab.fnItem("m") '输出某项属性值,等同输出 ab.fn.m 的值
	'-------------------------------------------------------------------------------------------

	Public Function FnItem(ByVal s)
		On Error Resume Next
		If IsObject(o_fn(s)) Then : Set FnItem = o_fn(s) : Else : FnItem = o_fn(s) : End If
		On Error Goto 0
	End Function
	Public Function FnAttr(ByVal s):FnAttr = FnItem(s):End Function

	'-------------------------------------------------------------------------------------------
	'# AB.FnItems()
	'# @return: 字典对象(Dictionary)
	'# @dowhat: 获取AB.Fn拓展的所有属性对象的集合信息，该信息返回的是一个字典(Dictionary)对象
	'--DESC-------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO-------------------------------------------------------------------------------------
	'# class cls_t: function foo() : foo = "hello!" : end function : end class
	'# class cls_a: function sum(a,b) : sum = a + b : end function : end class
	'# dim o_t : set o_t = New cls_t
	'# dim o_a : set o_a = New cls_a
	'# ab.fnAdd "m", o_t
	'# ab.fnAdd "n", o_a
	'# AB.C.PrintCn ab.fn.m.foo() '输出：hello!
	'# AB.C.PrintCn ab.fn.n.sum(2,5) '输出：7
	'# AB.C.PrintCn ab.fnHas("m") '输出：True
	'# AB.C.PrintCn ab.fnHas("abc") '输出：False
	'# AB.Trace ab.fnItems '输出全部定义属性值(字典对象)
	'# AB.Trace ab.fnItem("m") '输出属性 m 的值
	'-------------------------------------------------------------------------------------------

	Public Function FnItems()
		On Error Resume Next
		Set FnItems = o_fn
		On Error Goto 0
	End Function

	Public Sub setError(ByVal eStr, ByVal eCode)
		On Error Resume Next
		[Error].Msg = eStr & ""
		[Error].Raise eCode
		On Error Goto 0
	End Sub

	Public Sub ShowErr(ByVal ErrCode, ByVal ErrDesc)
		Err.Clear
		Dim strOut : strOut = "<style>.ab-showerr{width:400px;font-size:12px;font-family:Consolas;margin:10px auto;padding:0;background-color:#FFF;}.ab-showerr h3,.ab-showerr h4{font-size:12px;margin:0;line-height:24px;text-align:center;background-color:#999;border:1px solid #555;color:#FFF;border-bottom:none;}.ab-showerr h4{padding:5px;line-height:1.5em;text-align:left;background-color:#FFC;color:#000; font-weight:normal;}.ab-showerr h4 strong{color:red;}.ab-showerr table{width:100%;margin:0;padding:0;border-collapse:collapse;border:1px solid #555;border-bottom:none;}.ab-showerr th{background-color:#EEE;white-space:nowrap;}.ab-showerr thead th{background-color:#CCC;}.ab-showerr th,.ab-showerr td{font-size:12px;border:1px solid #999;padding:6px;line-height:20px;word-break:break-all;}.ab-showerr span.info{color:#F30;}</style><div class=""ab-showerr""><h3>Microsoft VBScript 编译器错误</h3><h4>程序出错了，"& AB_IIF(ErrCode<>"", "错误代码： <strong>"&ErrCode&"</strong>，", "") &"以下是错误描述：</h4><table><tr><td>"&ErrDesc&"</td></tr></table></div>"
		Pub.Put strOut
	End Sub

	'------------------------------------------------------------------------------------------------------

		Private Function AB_IIF(ByVal Cn, ByVal T, ByVal F)
			If Cn Then
				AB_IIF = T
			Else
				AB_IIF = F
			End If
		End Function

		Private Function AB_FixAbsPath(ByVal p)
			p = AB_IIF(Left(p,1)= "/", p, "/" & p)
			p = AB_IIF(Right(p,1)="/", p, p & "/")
			AB_FixAbsPath = p
		End Function

		Private Function AB_ExtLoaded(Byval f) '判断插件是否已载入
			On Error Resume Next
			Dim loaded : loaded = False
			f = Lcase(Pub.RegReplace(f,"^\[(.*)\]$","$1"))
			If Not o_ext.Exists(f) Then
				loaded = False
			Else
				If LCase(TypeName(o_ext(f))) = "cls_ab_" & f Then
					loaded = True
				Else
					Dim fname:fname=LCase(TypeName(Eval("["&f&"]")))
					IF Err Then
						loaded = False
						Err.Clear
					Else
						If LCase(TypeName(Eval("["&f&"]"))) = "cls_ab_" & f Then
							loaded = True
						End IF
					End IF
				End If
			End If
			AB_ExtLoaded = loaded
			On Error Goto 0
		End Function

		Private Function AB_appendCore(ByVal s)
			Dim old_cores, new_cores
			old_cores = Trim(s_cores)
			If old_cores="" Then
				If Trim(s)<>"" Then
					new_cores = "[" & Trim(Pub.RegReplace(Trim(s),"^\[(.*)\]$","$1")) & "]"
				Else
					new_cores = ""
				End If
			Else
				Dim I,K,Ta,tmp:K=0:Ta=Split(old_cores,",")
				For I=0 To Ubound(Ta)
					If Trim(Ta(I))<>"" Then
						K=K+1
						If K=1 Then
							tmp = tmp & "" & Trim(Ta(I)) & ""
						Else
							tmp = tmp & "," & Trim(Ta(I)) & ""
						End If
					End If
				Next
				old_cores = tmp
				If Trim(s)="" Then
					new_cores = old_cores
				Else
					If old_cores="" Then
						new_cores = "[" & Trim(Pub.RegReplace(Trim(s),"^\[(.*)\]$","$1")) & "]"
					Else
						new_cores = old_cores & "," & "[" & Trim(Pub.RegReplace(Trim(s),"^\[(.*)\]$","$1")) & "]"
					End If
				End If
			End If
			AB_appendCore = new_cores
		End Function

		Private Sub AB_FixBaseCore()
			Dim old_cores, new_cores, no_cores, temp, e, a, b, m, n, i, k, p, aa(), ab()
			old_cores = Trim(s_cores) : no_cores = Trim(s_nocores) : k = 0 : p = 0
			old_cores = "[" & Trim(Pub.RegReplace(Trim(old_cores),"^\[(.*)\]$","$1")) & "]"
			no_cores = "[" & Trim(Pub.RegReplace(Trim(no_cores),"^\[(.*)\]$","$1")) & "]"
			a = Split(Trim(old_cores),",") : b = Split(Trim(no_cores),",")
			m = UBound(a) : n = UBound(b)
			If n>=0 Then
				For i=0 To UBound(b)
					temp = Trim(Pub.RegReplace(Trim(b(i)),"^\[(.*)\]$","$1"))
					temp = Trim(Replace(Replace(temp,"[",""),"]",""))
					If temp<>"" Then
						Redim Preserve ab(p)
						ab(p) = "[" & temp & "]"
						p = p+1
					End If
				Next
			End If
			If m>=0 Then
				For i=0 To UBound(a)
					temp = Trim(Pub.RegReplace(Trim(a(i)),"^\[(.*)\]$","$1"))
					temp = Trim(Replace(Replace(temp,"[",""),"]",""))
					If temp<>"" Then
						temp = "[" & temp & "]"
						If Not Pub.InArray(temp,ab) Then
							Redim Preserve aa(k)
							aa(k) = temp
							k = k+1
						End If
					End If
				Next
			End If
			new_cores = Join(aa,",")
			s_cores = Trim(new_cores)
		End Sub

		Public Sub AB_xInclude(ByVal path, ByVal f)
			On Error Resume Next
			Err.Clear
			Dim p, fpath, fname, a, scode : f = LCase(f)
			Dim mdb, dbpath,connstr : mdb = "ab.libs.mdb" : dbpath = s_cachePath & mdb
			Dim tempconnstr,o_fso,o_ca,o_je,conn,sql,rs,temppath : temppath = s_cachePath&"temp.mdb"
			p = Replace(path,"\","/")
			fpath = Left(p, InstrRev(p,"/"))
			fname = Mid(p, InstrRev(p,"/")+1, Len(p))
			If Mid(dbpath,2,1)<>":" Then dbpath = Server.MapPath(dbpath)
			If Mid(temppath,2,1)<>":" Then temppath = Server.MapPath(temppath)
			connstr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="&dbpath&";"
			tempconnstr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="&temppath&";"
			If LCase(s_cacheType) = "app" Then
				If Application("ab."&f)="" Then
					Application.Lock
					scode = Pub.IncCode(path)
					a = Pub.FileInfo(path)
					Application("ab."&f) = scode
					Application("ab."&f&":"&"last") = a(5)
					Application.UnLock
				Else
					a = Pub.FileInfo(path)
					If Not Application("ab."&f&":"&"last") = a(5) Then
						Application.Lock
						scode = Pub.IncCode(path)
						Application("ab."&f) = scode
						Application("ab."&f&":"&"last") = a(5)
						Application.UnLock
					Else
						scode = Application("ab."&f)
					End If
				End If
			ElseIf LCase(s_cacheType) = "file" Then
				If Not Pub.isFile(s_cachePath & fname) Then
					scode = Pub.IncCode(path)
					Pub.SaveFile (s_cachePath & fname), "<"&"%"& scode &"%"&">"
					a = Pub.FileInfo(path)
					Pub.SetFileModifyDate (s_cachePath & fname), a(5)
				Else
					a = Pub.FileInfo(path)
					b = Pub.FileInfo(s_cachePath & fname)
					If Not b(5) = a(5) Then
						scode = Pub.IncCode(path)
						Pub.SaveFile (s_cachePath & fname), "<"&"%"& scode &"%"&">"
						Pub.SetFileModifyDate (s_cachePath & fname), a(5)
					Else
						scode = Pub.IncCode(s_cachePath & fname)
					End If
				End If
			ElseIf LCase(s_cacheType) = "data" Then
				If Not Pub.dbExists(dbpath) Then
					scode = Pub.IncCode(path)
					a = Pub.FileInfo(path)
					Set o_fso = Server.CreateObject(s_fsoName)
					If o_fso.FileExists(dbpath) Then o_fso.DeleteFile(dbpath)
					Set o_fso = Nothing
					Set o_ca = Server.CreateObject("ADOX.Catalog")
					o_ca.Create tempconnstr
					Set o_ca = Nothing
					Set o_je = Server.CreateObject("JRO.JetEngine")
					o_je.CompactDatabase tempconnstr,connstr
					Set o_fso = Server.CreateObject(s_fsoName)
					o_fso.DeleteFile(temppath)
					Set o_je   = Nothing
					Set o_fso  = Nothing
					Set o_cache_conn = Server.CreateObject("ADODB.Connection")
					Application.Lock
					Set Application("ab:cache-conn") = o_cache_conn
					Application.UnLock
					Set rs = Server.CreateObject("ADODB.Recordset")
					o_cache_conn.open connstr
					sql = "CREATE TABLE [lists] ([id] int IDENTITY (1, 1) PRIMARY KEY NOT NULL,[name] Varchar(50) NOT NULL,[content] Text NOT NULL,[lasttime] DATETIME)"
					o_cache_conn.Execute(sql)
					sql = "Select top 1 * From [lists] where [name]='"&fname&"'"
					rs.open sql,o_cache_conn,1,3
					If Not Rs.Eof Then
						rs("name")=fname
						rs("content")=scode
						rs("lasttime")=a(5)
						rs.update
					Else
						rs.Close
						sql = "Select * From [lists] where id is null"
						rs.open sql,o_cache_conn,1,3
						rs.addnew
						rs("name")=fname
						rs("content")=scode
						rs("lasttime")=a(5)
						rs.update
					End If
					rs.Close : Set rs = Nothing
				Else
					If Not IsObject(Application("ab:cache-conn")) Or TypeName(Application("ab:cache-conn"))<>"Connection" Then
						Set o_cache_conn = Server.CreateObject("ADODB.Connection")
						o_cache_conn.open connstr
						If Err.Number<>0 Then Set o_cache_conn = Application("ab:cache-conn")
					Else
						Set o_cache_conn = Application("ab:cache-conn")
						o_cache_conn.open connstr
					End If
					Set rs = Server.CreateObject("ADODB.Recordset")
					rs.open "Select top 1 * From [lists] where [name]='"&fname&"'",o_cache_conn,1,1
					If Not rs.Eof Then
						a = Pub.FileInfo(path)
						If rs("lasttime")=a(5) Then
							scode = rs("content")
						Else
							rs.Close
							scode = Pub.IncCode(path)
							rs("lasttime")=a(5)
							rs.update
						End If
					Else
						rs.Close
						scode = Pub.IncCode(path)
						a = Pub.FileInfo(path)
						sql = "Select * From [lists]"
						rs.open sql,o_cache_conn,1,3
						rs.addnew
						rs("name")=fname
						rs("content")=scode
						rs("lasttime")=a(5)
						rs.update
					End If
					rs.Close : Set rs = Nothing
				End If
			Else
				scode = Pub.IncCode(path)
			End If
			ExecuteGlobal scode
			On Error Goto 0
		End Sub

End Class
Class Cls_AB_Obj : End Class

Class Cls_AB_Pub
	Private s_fsoName, s_steamName, s_charset, s_bom
	Private o_regex, o_fso
	Private b_charset, b_clearmark

	Private Sub Class_Initialize()
		On Error Resume Next
		s_fsoName 			= AB.fsoName
		s_steamName 		= AB.steamName
		s_charset 			= AB.CharSet
		s_bom		 		= AB.FileBOM
		b_charset		 	= AB.ifCharSet
		b_clearmark		 	= AB.ifClearMark
		Set o_regex 		= New Regexp
		o_regex.Global 		= True
		o_regex.IgnoreCase 	= True
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Set o_regex = Nothing
	End Sub

	Public Function FixAbsPath(ByVal p)
		p = IIF(Left(p,1)= "/", p, "/" & p)
		p = IIF(Right(p,1)="/", p, p & "/")
		FixAbsPath = p
	End Function

	Public Function Print(Str)
		Response.Write Str
	End Function

	Public Function Put(Str)
		Response.Write Str
		Set AB = Nothing
		Response.End()
	End Function

	Public Function toNumber(ByVal n, ByVal d)
		toNumber = FormatNumber(n,d,-1)
	End Function

	Public Function GetScriptTime(t)
		If IsNull(t) Or t&"" = "" Or t&"" = "0" Then t = Abx_Timer
		GetScriptTime = FormatNumber((Timer()-t)*1000, 2, -1)
	End Function

	Public Function isInstr(Byval t, Byval s)
		Dim rt : rt = False
		Dim ta : ta = Replace(t & "", "|", ",")
		Dim I
		For I=0 To Ubound(Split(ta,","))
			IF Trim(s) = Split(ta,",")(I) Then:rt = True:Exit For:End IF
		Next
		isInstr = rt
	End Function

	Public Function IIF(ByVal Cn, ByVal T, ByVal F)
		If Cn Then
			IIF = T
		Else
			IIF = F
		End If
	End Function

	Public Function isNul(ByVal s)
		On Error Resume Next:IF Err.Number<>0 Then Err.Clear
		isNul = False
		Select Case VarType(s)
			Case vbEmpty, vbNull
				isNul = True : Exit Function
			Case vbString
				If s="" Then isNul = True : Exit Function
			Case vbObject
				Select Case TypeName(s)
					Case "Nothing","Empty"
						isNul = True : Exit Function
					Case "Recordset"
						If s.State = 0 Then isNul = True : Exit Function
						If s.Bof And s.Eof Then isNul = True : Exit Function
					Case "Dictionary"
						If s.Count = 0 Then isNul = True : Exit Function
				End Select
			Case vbArray,8194,8204,8209
				If Ubound(s)=-1 Then isNul = True : Exit Function
		End Select
		On Error Goto 0
	End Function

	Public Function Has(ByVal s)
		On Error Resume Next:IF Err.Number<>0 Then Err.Clear
		Has = Not isNul(s)
		On Error Goto 0
	End Function

	Public Function IfThen(ByVal Cn, ByVal T)
		IfThen = IIF(Cn,T,"")
	End Function

	Public Function IfHas(ByVal v1, ByVal v2)
		IfHas = IIF(Has(v1), v1, v2)
	End Function

	Public Function isFile(ByVal p)
		isFile = False
		Dim o_fso : Set o_fso = Server.CreateObject(s_fsoName)
		If Mid(p,2,1)<>":" Then p = Server.MapPath(p)
		If o_fso.FileExists(p) Then isFile = True
	End Function

	Public Function isFolder(ByVal p)
		isFolder = False
		Dim o_fso : Set o_fso = Server.CreateObject(s_fsoName)
		If Mid(p,2,1)<>":" Then p = Server.MapPath(p)
		If o_fso.FolderExists(p) Then isFolder = True
	End Function

	Public Function CLeft(ByVal s, ByVal m)
		CLeft = LR(s,m,0)
	End Function

	Public Function CRight(ByVal s, ByVal m)
		CRight = LR(s,m,1)
	End Function

	Public Function LR(ByVal s, ByVal m, ByVal t)
		Dim n : n = Instr(s,m)
		If n>0 Then
			If t = 0 Then
				LR = Left(s,n-1)
			ElseIf t = 1 Then
				LR = Mid(s,n+Len(m))
			End If
		Else
			If t = 0 Then
				LR = s
			Else
				LR = ""
			End If
		End If
	End Function

	Public Function RegMatch(ByVal s, ByVal rule)
		o_regex.Pattern = rule
		Set RegMatch = o_regex.Execute(s)
		o_regex.Pattern = ""
	End Function

	Public Function RegReplace(ByVal s, ByVal rule, Byval Result)
		RegReplace = ReplaceX(s,rule,Result,0)
	End Function

	Public Function RegReplaceM(ByVal s, ByVal rule, Byval Result)
		RegReplaceM = ReplaceX(s,rule,Result,1)
	End Function

	Public Function ReplaceX(ByVal s, ByVal rule, Byval Result, ByVal isM)
		Dim tmpStr,Reg : tmpStr = s
		If Has(s) Then
			If isM = 1 Then o_regex.Multiline = True
			o_regex.Pattern = rule
			tmpStr = o_regex.Replace(tmpStr,Result)
			If isM = 1 Then o_regex.Multiline = False
			o_regex.Pattern = ""
		End If
		ReplaceX = tmpStr
	End Function

	Public Function RegTest(ByVal s, ByVal p)
		If isNul(s) Then RegTest = False : Exit Function
		o_regex.Pattern = p
		RegTest = o_regex.Test(CStr(s))
		o_regex.Pattern = ""
	End Function

	Public Function InArray(Byval s, Byval arr)
		Dim x : InArray = False
		For Each x In arr
			If x = s Then
				InArray = True
				Exit For
			End If
		Next
	End Function

	Public Sub Include(ByVal filePath)
		On Error Resume Next
		IF Err.Number<>0 Then Err.Clear
		Dim tempCode : tempCode = IncCode(filePath)
		ExecuteGlobal tempCode
		If Err.Number<>0 Then
			AB.setError "(Error File: """ & filePath & """)", 2
		End If
		On Error Goto 0
	End Sub

	Public Sub xInclude(ByVal filePath)
		On Error Resume Next
		IF Err.Number<>0 Then Err.Clear
		ExecuteGlobal IncCode(filePath)
		On Error Goto 0
	End Sub

	Public Function IncCode(ByVal filePath)
		On Error Resume Next
		IncCode = GetIncCode(IncRead(filePath),0)
		On Error Goto 0
	End Function

	Public Function IncXCode(ByVal filePath)
		On Error Resume Next
		IncXCode = GetIncCode(IncRead(filePath),1)
		On Error Goto 0
	End Function

	Public Function GetIncCode(ByVal content, ByVal getHtml)
		On Error Resume Next
		IF Err.Number<>0 Then Err.Clear
		Dim tmpStr,code,tmpCode,s_code,st,en,inc,Match,tmpCont
		s_code = "Abx_s_html = """" "
		'去除asp脚本块〈%  %〉中还含有〈%  %〉等字符
		If b_clearmark Then '如果开启会耗很大资源
			''content = RegReplaceM(content,"<"&"%([\s\S]*?)([\s]*?('|rem)\s+?(<"&"%"&".*?"&"%"&">)+?[ \t]*[\r\n])([\s\S]*?)%"&">", "<"&"%"& "$1 $5" &"%"&">")
			content = RegReplaceM(content,"<"&"%([\s\S]*?)(<"&"%"&"[\s\S]*?"&"%"&">)+([\s\S]*?)%"&">", "<"&"%"& "$1$3" &"%"&">")
			content = RegReplaceM(content,"<"&"%([\s\S]*?)((<"&"%"&")+.*([\r\n]?))([\s\S]*?)%"&">", "<"&"%"& "$1$4$5" &"%"&">")
			content = RegReplaceM(content,"<"&"%([\s\S]*?)(("&"%"&">)+)([\s\S]*?)%"&">", "<"&"%"& "$1$4" &"%"&">")
		End If
		code = "" : st = 1 : en = Instr(content,"<"&"%") + 2
		s_code = IIF(getHtml=1,"Abx_s_html = Abx_s_html & ","Response.Write ")
		Do While en > st + 1
			tmpStr = Mid(content,st,en-st-2)
			st = Instr(en,content,"%"&">") + 2
			If Has(tmpStr) Then
				tmpStr = Replace(tmpStr,"""","""""")
				tmpStr = Replace(tmpStr,vbCrLf,"""&vbCrLf&""")
				'修正(UNIX ANSI文件)内容无法获取的Bug
				tmpStr = Replace(tmpStr,Chr(10),"""&vbCrLf&""")
				tmpStr = Replace(tmpStr,Chr(13),"")
				code = code & s_code & """" & tmpStr & """" & vbCrLf
			End If
			tmpStr = Mid(content,en,st-en-2)
			tmpCode = tmpStr
			tmpCode = RegReplaceM(tmpCode, "^\s*[\r\n]+\s*$", "") '删除空行
			tmpCode = RegReplaceM(tmpCode, "^\s*[\r\n]+\s*(rem|').*$", "") '去掉空白行注释
			'tmpCode = RegReplaceM(tmpCode, "(\s+?rem[\t ]+|\s*\')((?!"").)*$", "") '去掉行尾注释
			If False Then '这个太消耗资源,严重影响速度。暂时屏蔽。
				tmpCode = RegReplaceM(tmpCode, "^\s*(.*)\s*$", "$1") '删除首空格以及空行
				tmpCode = RegReplaceM(tmpCode, "^(.*?)[ \t]+$", "$1") '删除尾空格
			End If
			tmpCode = RegReplace(tmpCode,"^\s*=\s*",s_code) & vbCrLf
			If getHtml = 1 Then
				tmpCode = RegReplaceM(tmpCode,"Response\.Write([\( ])", s_code & "$1") & vbCrLf
				tmpCode = RegReplaceM(tmpCode,"AB\.C\.Echo([\( ])", s_code & "$1") & vbCrLf '为了应用echo()函数
				tmpCode = RegReplaceM(tmpCode,"AB\.C\.Print([\( ])", s_code & "$1") & vbCrLf '为了应用print()函数
			End If
			code = code & tmpCode
			en = Instr(st,content,"<"&"%") + 2
		Loop
		tmpStr = Mid(content,st)
		If Has(tmpStr) Then
			tmpStr = Replace(tmpStr,"""","""""")
			tmpStr = Replace(tmpStr,vbcrlf,"""&vbCrLf&""")
			'以下这两句,修正了(UNIX ANSI文件)内容无法获取的Bug
			tmpStr = Replace(tmpStr,Chr(10),"""&vbCrLf&""")
			tmpStr = Replace(tmpStr,Chr(13),"")
			code = code & s_code & """" & tmpStr & """" & vbCrLf
		End If
		If getHtml = 1 Then code = "Abx_s_html = """" " & " : " & code
		code = RegReplace(code,"(\n\s*\r)+",vbCrLf)
		GetIncCode = code
	End Function

	Public Function IncRead(ByVal filePath)
		On Error Resume Next
		Dim content, rule, inc, Match, incFile, incStr
		content = Read(filePath)
		If isNul(content) Then Exit Function
		content = RegReplace(content,"<"&"%[\s]*?@.*?%"&">","")
		content = RegReplace(content,"(<"&"%[^>]+?)(option +?explicit)([^>]*?%"&">)","$1'$2$3")
		rule = "<!-- *?#include +?(file|virtual) *?= *?""??([^"":?*\f\n\r\t\v]+?)""?? *?-->"
		If RegTest(content,rule) Then
			Set inc = RegMatch(content,rule)
			For Each Match In inc
				If LCase(Match.SubMatches(0))="virtual" Then
					incFile = Match.SubMatches(1)
				Else
					incFile = Mid(filePath,1,InstrRev(filePath,IIF(Instr(filePath,":")>0,"\","/"))) & Match.SubMatches(1)
				End If
				incStr = IncRead(incFile)
				content = Replace(content,Match,incStr)
			Next
			Set inc = Nothing
		End If
		IncRead = content
		On Error Goto 0
	End Function

	Public Function Read(ByVal filePath)
		Dim p, o_strm, tmpStr, s_char, t
		s_char = s_charset
		If Instr(filePath,"|")>0 Then
			t = LCase(Trim(CRight(filePath,"|")))
			filePath = CLeft(filePath,"|")
		End If
		If Instr(filePath,">")>0 Then
			s_char = UCase(Trim(CRight(filePath,">")))
			filePath = Trim(CLeft(filePath,">"))
		End If
		p = filePath
		If Mid(p,2,1)<>":" Then p = Server.MapPath(p)
		If isFile(p) Then
			If b_charset=False Then
				s_char = GetFileCharset(p) '自动检测文件编码（当未设置编码时）
			End If
			IF s_char="" Then s_char = s_charset
			'IF UCase(s_char) = "GBK" Then s_char = "GB2312"
			Set o_strm = Server.CreateObject(s_steamName)
			With o_strm
				.Type = 2
				.Mode = 3
				.Open
				.LoadFromFile p
				.Charset = s_char
				.Position = 2
				tmpStr = .ReadText
				.Close
			End With
			Set o_strm = Nothing
			If s_char = "UTF-8" Then
				Select Case s_bom
					Case "keep"
						tmpStr = tmpStr & ""
					Case "remove"
						If RegTest(tmpStr, "^\uFEFF") Then
							tmpStr = RegReplace(tmpStr, "^\uFEFF", "")
						End If
					Case "add"
						If Not RegTest(tmpStr, "^\uFEFF") Then
							tmpStr = Chrw(&hFEFF) & tmpStr
						End If
				End Select
			End If
		Else
			tmpStr = ""
			If isNul(t) Then
				tmpStr = "File Not Found: '" & filePath & "'"
			ElseIf t="fso" Then
				AB.setError "(File Not Found: """ & filePath & """)", 3
			End If
		End If
		Read = tmpStr
	End Function

	Public Function GetFileCharset(ByVal FilePath)
		On Error Resume Next
		IF Err Then:Err.Clear
		If InStr(FilePath,":")=0 Then
			FilePath=Server.MapPath(FilePath)
		End If
		Dim objStream,LoadBytes
		Set objStream = CreateObject(s_steamName)
			With objStream
				.Type = 1
				.Mode = 3
				.Open
				.LoadFromFile FilePath
				LoadBytes=.Read(3)
				.Close
			End With
		Set objStream = NoThing
		IF Err Then:Err.Clear:GetFileCharset="":Exit Function:End IF
		Dim FileCharset, strFileHead
		strFileHead=BinToHex(LoadBytes)
		If strFileHead = "EFBBBF" Then
			GetFileCharset = "UTF-8"
		Else
			strFileHead = Left(strFileHead, 4)
			If strFileHead = "FEFF" Then
				GetFileCharset = "UNICODE BIG"
			ElseIf strFileHead = "FFFE" Then
				GetFileCharset = "UNICODE"
			ElseIf strFileHead = "3C25" Then
				GetFileCharset = "GB2312"
			Else
				GetFileCharset = "GB2312"
			End If
		End If
		IF Err Then:Err.Clear:GetFileCharset="":Exit Function:End IF
		On Error Goto 0
	End Function

	Public Function BinToHex(ByVal vStream)
		Dim reVal,I
		reVal = 0
		For I = 1 To LenB(vStream)
			reVal = reVal * 256 + AscB(MidB(vStream,I,1))
		Next
		BinToHex=Hex(reVal)
	End Function

	Public Function RemoveBOM__(ByVal path) '去掉BOM信息
		If InStr(path,":")=0 Then
			path=Server.MapPath(path)
		End If
		Dim o_stm
		Set o_stm = CreateObject(s_steamName)
			With o_stm
				.Type = 1
				.Mode = 3
				.Open
				.LoadFromFile path
				.Position = 3
				.Write = .Read
				.SaveToFile path,2
				.Close
			End With
		Set o_stm = Nothing
	End Function

	Public Function RemoveBOM(ByVal path) 'Remove Bom
		On Error Resume Next
		Dim o_stm, str
		Set o_stm = Server.CreateObject(s_steamName)
			With o_stm
				.Type = adTypeBinary
				.Mode = adModeReadWrite
				.Open
				.Position = o_stm.Size
				.LoadFromFile path
				.Position = 3
				str = .Read
				.Close
			End With
		Set o_stm = NoThing
		Set o_stm = Server.CreateObject(s_steamName)
			With o_stm
				.Type = adTypeBinary
				.Mode = adModeReadWrite
				.Open
				.Position = o_stm.Size
				.Write = str
				.SaveToFile path,adSaveCreateOverWrite
				.Close
			End With
		Set o_stm = Nothing
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Pub.FileInfo(path)
	'# @return: Array (数组)
	'# @dowhat: 获取文件信息数组
	'#  语法：arr = AB.Pub.FileInfo(path)
	'#  arr(0) : 文件的名称
	'#  arr(1) : 文件的路径
	'#  arr(2) : 文件的所在驱动器
	'#  arr(3) : 文件的创建日期
	'#  arr(4) : 文件的最后浏览日期
	'#  arr(5) : 文件的最后修改日期
	'#  arr(6) : 文件的大小
	'#  arr(7) : 文件的类型
	'#  arr(8) : 文件的属性
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串) 文件路径
	'--DEMO------------------------------------------------------------------------------------
	'# Dim a : a = AB.Pub.FileInfo("/news/list.html")
	'# AB.C.Print a(5) '输出：2013-2-23 16:05:46 即文件的最后修改日期
	'------------------------------------------------------------------------------------------

	Public Function FileInfo(ByVal p)
		On Error Resume Next
		Dim f, path, arrInfo(8) : path = Trim(p)
		If Mid(path,2,1)<>":" Then path = Server.MapPath(path)
		Dim o_fso : Set o_fso = Server.CreateObject(s_fsoName)
		If o_fso.FileExists(path) Then
			Set f = o_fso.GetFile(path)
			arrInfo(0) = f.Name
			arrInfo(1) = f.Path
			arrInfo(2) = f.Drive
			arrInfo(3) = f.DateCreated
			arrInfo(4) = f.DateLastAccessed
			arrInfo(5) = f.DateLastModified
			arrInfo(6) = f.Size
			arrInfo(7) = f.Type
			arrInfo(8) = f.Attributes
		End If
		Set f = Nothing
		FileInfo = arrInfo
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Pub.FileAttr(path)
	'# @syntax: attr = AB.Pub.FileAttr(path)
	'# @return: Array (数组)
	'# @dowhat: 获取文件的属性
	'#   文件的属性 attr 值相关：
	'#    常数         值       描述
	'#     Normal       0    普通文件。不设置属性。
	'#     ReadOnly     1    只读文件。属性为读/写。
	'#     Hidden       2    隐藏文件。属性为读/写。
	'#     System       4    系统文件。属性为读/写。
	'#     Volume       8    磁盘驱动器卷标。属性为只读。
	'#     Directory    16   文件夹或目录。属性为只读。
	'#     Archive      32   文件在上次备份后已经修改。属性为读/写。
	'#     Alias        64   链接或者快捷方式。属性为只读。
	'#     Compressed   128  压缩文件。属性为只读。
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串) 文件路径
	'--DEMO------------------------------------------------------------------------------------
	'# none
	'------------------------------------------------------------------------------------------

	Public Function FileAttr(Byval p)
		On Error Resume Next
		Dim arrInfo : arrInfo = FileInfo(p)
		FileAttr = arrInfo(8)
		On Error Goto 0
	End Function

	Public Function SaveFile(ByVal p, ByVal s)
		On Error Resume Next
		Dim path : path = Trim(p)
		If Mid(path,2,1)<>":" Then path = Server.MapPath(path)
		If SaveFolder(Left(path,InstrRev(path,"\")-1)) Then
			Dim o_strm : Set o_strm = Server.CreateObject(s_steamName)
			With o_strm
				.Type = 2
				.Open
				.Charset = s_charset
				.Position = o_strm.Size
				.WriteText = s
				.SaveToFile path,2
				.Close
			End With
			Set o_strm = Nothing
		End If
		If Err Then SaveFile = False
		On Error Goto 0
	End Function

	Public Function SaveFolder(ByVal p)
		On Error Resume Next
		SaveFolder = True
		Dim o_fso : Set o_fso = Server.CreateObject(s_fsoName)
		Dim path : path = Trim(p)
		If Mid(path,2,1)<>":" Then path = Server.MapPath(path)
		If Not isFolder(path) Then o_fso.CreateFolder(path)
		If Err Then SaveFolder = False
		On Error Goto 0
	End Function

	Public Function CopyFile(ByVal fromPath, ByVal toPath)
		On Error Resume Next
		Dim o_fso : Set o_fso = Server.CreateObject(s_fsoName)
		If Mid(fromPath,2,1)<>":" Then fromPath = Server.MapPath(fromPath)
		If Mid(toPath,2,1)<>":" Then toPath = Server.MapPath(toPath)
		o_fso.CopyFile fromPath,toPath
		On Error Goto 0
	End Function

	Public Function SetFileModifyDate(ByVal f, ByVal t)
		On Error Resume Next
		Dim shellapp, app_path, app_file
		Set shellapp = Server.CreateObject("Shell.Application")
		Dim p : p = Trim(f)
		If Mid(p,2,1)<>":" Then p = Server.MapPath(p)
		Dim fname : fname = Mid(p, InstrRev(p,"\")+1, Len(p))
		Dim fpath : fpath = Left(p, InstrRev(p,"\"))
		If Not isFolder(fpath) Then SaveFolder(fpath)
		Set app_path = shellapp.NameSpace(fpath)
		Set app_file = app_path.ParseName(fname)
		app_file.Modifydate = CDate(t) '修改文件的最后修改时间
		Set shellapp = Nothing
		Set app_path = Nothing
		Set app_file = Nothing
		On Error Goto 0
	End Function

	Public Function dbExists(ByVal path)
		On Error Resume Next
		Dim c : Set c = Server.CreateObject("ADODB.Connection")
		If Mid(path,2,1)<>":" Then path = Server.MapPath(path)
		c.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & path
		If Err.Number<>0 Then
			Err.Clear
			dbExists = False
		Else
			dbExists = True
		End If
		Set c = Nothing
		On Error Goto 0
	End Function

	'------------------

	'判断核心是否已载入
	Public Function IsLoaded_Core(Byval ex, Byval f, ByRef o)
		'On Error Resume Next
		Dim i, a, t, fname, loaded : loaded = False
		f = Lcase(AB.C.RegReplace(f,"^\[(.*)\]$","$1"))
		If Not o.Exists(f) Then : IsLoaded_Core = False : Exit Function : End If
		If Trim(ex)<>"" Then
			a = Split(ex,".")
			For i=0 To UBound(a)
				If Trim(a(i))<>"" Then t = t & Trim(a(i)) & "_"
			Next
		End If
		'If Lcase(TypeName(o(""& f))) = "cls_"& t &"obj" Then loaded = False
		'If Lcase(TypeName(o(""& f))) <> "cls_"& t & f Then loaded = False
		fname = Eval("LCase(TypeName("& (ex &".["&f&"]") &"))")
		IF Err Then
			loaded = False
			Err.Clear
		Else
			'If Eval("LCase(TypeName("& (ex &".["&f&"]") &"))") = "empty" Then loaded = False
			'If Eval("LCase(TypeName("& (ex &".["&f&"]") &"))") = "cls_"& t &"obj" Then loaded = False
			If Eval("LCase(TypeName("& (ex &".["&f&"]") &"))") = "cls_"& t & f Then
				If Lcase(TypeName(o(""& f))) = "cls_"& t & f Then loaded = True
			End IF
		End IF
		IsLoaded_Core = loaded
		On Error Goto 0
	End Function

	Public Sub CoreInclude(Byval ex, Byval f, ByVal o, ByVal p)
		On Error Resume Next
		IF (Not IsLoaded_Core(ex, f, o_lib)) Then
			'IF (IsCoreFileIncluded(ex,f)=False) Then Include p
			xInclude p
		End IF
		On Error Goto 0
	End Sub

	Public Function IsCoreFileIncluded(Byval ex, Byval f)
		On Error Resume Next
		Dim IsHasExits:IsHasExits=False
		Dim i, a, t, tpljxTaObj, tpljxTaName
		If Trim(ex)<>"" Then
			a = Split(ex,".")
			For i=0 To UBound(a)
				If Trim(a(i))<>"" Then t = t & Trim(a(i)) & "_"
			Next
		End If
		'ExecuteGlobal "Set tpljxTaObj = New cls_"& t & f
		'Execute "tpljxTaName = TypeName(New cls_"& t & f & ")"
		tpljxTaName = Eval("TypeName(New cls_"& t & f & ")")
		IF Err.Number=0 Then:IsHasExits=True:Err.Clear:Else:IsHasExits=False:End IF
		IF IsObject(tpljxTaObj) Then Set tpljxTaObj=Nothing
		IsCoreFileIncluded = IsHasExits
		On Error Goto 0
	End Function

	Public Function CoresArray(ByVal s)
		Dim i, a(), arr, temp
		If Trim(s)="" Then : CoresArray = Array(): Exit Function : End If
		arr = Split(s,",")
		For i = 0 To UBound(arr)
			Redim Preserve a(i)
			temp = arr(i)
			temp = AB.C.RegReplace(Trim(temp),"^\[(.*)\]$","$1")
			a(i) = Lcase(Trim(temp))
		Next
		CoresArray = a
	End Function

End Class
%>