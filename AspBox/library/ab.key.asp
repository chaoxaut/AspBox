<%
'######################################################################
'## ab.key.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Dictionary Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2013/03/05 1:16
'## Description :   AspBox字典库操作类
'######################################################################

Class Cls_AB_Key

	Private o_dict
	Public Items

	Private Sub Class_Initialize()
		Set Items = Server.CreateObject(AB.dictName)
	End Sub

	Private Sub Class_Terminate()
		Set Items = Nothing
	End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Key.New 方法
	'# @syntax: Set key = AB.Key.New
	'# @return: Object (ASP对象)
	'# @dowhat: 建立字典操作对象, 创建一个 AspBox_Key 对象
	'--DESC------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO------------------------------------------------------------------------------------
	'# none
	'------------------------------------------------------------------------------------------

	Public Function [New]()
		Set [New] = New Cls_AB_Key
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Key.Count 属性
	'# @syntax: n = AB.Key.Count
	'# @return: Integer (整型)
	'# @dowhat: 获取存储的key子项个数
	'--DESC------------------------------------------------------------------------------------
	'# @param n : Variant(已定义的变量)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Key.Clear '清空数据
	'# AB.Key("a") = "test123"
	'# AB.Key("b") = AB.Dict
	'# AB.C.PrintCn AB.Key.Count '计算个数: 2
	'------------------------------------------------------------------------------------------

	Public Property Get Count()
		Count = Items.Count
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Key.Item 方法
	'# @syntax: AB.Key.Item(str)[ = value]
	'# @alias:  AB.Key(str)[ = value]
	'# @return: Integer (整型)
	'# @dowhat: 设置或获取字典项值
	'--DESC------------------------------------------------------------------------------------
	'# @param str :  String (字符串) 存储的键名
	'# @param value(可选) : Any (任意值) 存储值
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Key("a") = "test123" '等效于：AB.Key("a").Set "test123"
	'# AB.C.PrintCn AB.Key("a") '字符串数据可以直接获取
	'# AB.Key("b") = AB.Dict '存储对象
	'# AB.Trace AB.Key("b").Value '读取对象值一般用 AB.Key(s).Value 方法获取
	'------------------------------------------------------------------------------------------

	Public Default Property Get Item(ByVal k)
		If Not IsObject(Items(k)) Then
			Set Items(k) = New Cls_AB_Key_Info
			Items(k).Key = k
		End If
		Set Item = Items(k)
	End Property

	Public Property Let Item(ByVal k, ByVal v)
		Add k, v
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Key.Keys 属性
	'# @syntax: n = AB.Key.Keys
	'# @return: Array (数组)
	'# @dowhat: 返回一个包含所有项键名的数组
	'--DESC------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Key.Add "a","abc123"
	'# AB.Key("b") = Array()
	'# AB.Key("c") = AB.Dict
	'# AB.C.PrintCn AB.Key.Count '计算项个数
	'# Dim a : a = AB.Key.Keys() '返回一个包含所有项键名的数组
	'# AB.C.PrintCn "所有子项键名："
	'# Dim i
	'# For Each i In a
	'# 	AB.C.PrintCn i
	'# Next
	'# Dim k : k = "a"
	'# AB.C.PrintCn "子项键名为 "& k &" 的值："
	'# AB.Trace AB.Key(k).Value
	'------------------------------------------------------------------------------------------

	Public Property Get Keys()
		Dim i, n, a : a = Array()
		n = 0
		For Each i In Items
			Redim Preserve a(n)
			a(n) = Items(i).Key
			n = n+1
		Next
		Keys = a
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Key.Add 方法
	'# @syntax: AB.Key.Add str, value
	'# @alias:  AB.Key.Set str, value 或 AB.Key(str).Set value 或 AB.Key(str).Add value
	'# @return: Void
	'# @dowhat: 存储键名为str的值到字典里
	'--DESC------------------------------------------------------------------------------------
	'# @param str : String (字符串) 存储的键名
	'# @param value(可选) : Any (任意值) 存储值
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Key("a").Add "test123" '等同于：AB.Key("a").Set "test123"
	'# AB.C.PrintCn AB.Key("a")
	'# AB.Trace AB.Key("a").Value
	'------------------------------------------------------------------------------------------

	Public Sub Add(ByVal k, ByVal v)
		If IsNull(k) Then k = ""
		If Not IsObject(Items(k)) Then
			Set Items(k) = New Cls_AB_Key_Info
		End If
		Items(k).Key = k
		Items(k).Value = v
	End Sub
	Public Sub [Set](ByVal k, ByVal v) : Add k, v : End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Key.Remove 方法
	'# @syntax: AB.Key.Remove str
	'# @alias:  AB.Key.Del str
	'# @return: Void
	'# @dowhat: 从字典里删除键名为str项
	'--DESC------------------------------------------------------------------------------------
	'# @param str : String (字符串) 键名
	'--DEMO------------------------------------------------------------------------------------
	'# Dim key : Set key = AB.Key.New
	'# key.add "a","abc123"
	'# key("b") = Array()
	'# key("c") = AB.Dict
	'# AB.Trace key("b")
	'# AB.Trace key("c").Value
	'# key.Del "c"
	'# If Not key.Items.Exists("c") Then AB.C.PrintCn "项 c 已被删除"
	'# AB.C.PrintCn "共" & key.Count & "项"
	'# Dim i
	'# For Each i In key.Keys
	'# 	AB.C.PrintCn "子项键名为 "& i &" 的值："
	'# 	AB.Trace key(i).Value
	'# Next
	'------------------------------------------------------------------------------------------

	Public Sub Remove(ByVal k)
		Dim f : f = k
		If Items.Exists(f) Then
			If Lcase(TypeName(Items(f))) = "cls_ab_key_info" Then Items(f).Clear()
			Items.Remove(f)
		End IF
	End Sub
	Public Sub Del(ByVal k) : Remove k : End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Key.Clear 方法
	'# @syntax: AB.Key.Clear
	'# @alias:  AB.Key.RemoveAll
	'# @return: Void
	'# @dowhat: 清空所有字典项
	'--DESC------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO------------------------------------------------------------------------------------
	'# Dim key : Set key = AB.Key.New
	'# key.add "a","abc123"
	'# key("b") = Array()
	'# key("c") = AB.Dict
	'# key.Clear
	'# AB.Trace key.Count '计算项个数
	'------------------------------------------------------------------------------------------

	Public Sub Clear()
		Dim f
		For Each f In Items
			If Lcase(TypeName(Items(f))) = "cls_ab_key_info" Then Items(f).Clear()
			If IsObject(Items(f)) Then Set Items(f) = Nothing Else Items(f) = Empty
		Next
		Items.RemoveAll
	End Sub
	Public Sub RemoveAll() : Clear() : End Sub

End Class

Class Cls_AB_Key_Info

	Private o_value
	Public Key

	Private Sub Class_Initialize() : End Sub
	Private Sub Class_Terminate: Clear(): End Sub

	Public Sub Clear()
		If IsObject(o_value) Then Set o_value = Nothing
	End Sub

	Public Property Let [Value](ByVal v)
		[Set] v
	End Property

	Public Default Property Get [Value]()
		If IsObject(o_value) Then
			Select Case TypeName(o_value)
				Case "Recordset"
					Set [Value] = o_value.Clone
				Case Else
					Set [Value] = o_value
			End Select
		Else
			[Value] = o_value
		End If
	End Property

	Public Sub [Set](ByVal v)
		If IsObject(v) Then
			Select Case TypeName(v)
				Case "Recordset"
					Set o_value = v.Clone
				Case Else
					Set o_value = v
			End Select
		Else
			o_value = v
		End If
	End Sub
	Public Sub Add(ByVal v) : [Set] v : End Sub

End Class
%>